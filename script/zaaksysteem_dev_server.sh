#! /bin/bash

if [ ! -e "/opt/filestore/zaaksysteem" ]; then
    echo "First run. Creating filestore for 'zaaksysteem' instance."
    mkdir -p /opt/filestore/zaaksysteem/storage
    chown -R zaaksysteem:zaaksysteem /opt/filestore/zaaksysteem
fi

PERL=$(which perl)
exec su -c "${PERL} /opt/zaaksysteem/script/zaaksysteem_fastcgi.pl -e -l 0.0.0.0:9083 -n ${ZS_NUM_THREADS:-3} -M Zaaksysteem::ProcManager" zaaksysteem
