// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.events.dispatchEvent', function () {
    var doc = document;

    if (doc.dispatchEvent) {
      return function (element, event) {
        return element.dispatchEvent(event);
      };
    } else if (doc.fireEvent) {
      return function (element, event) {
        return element.fireEvent(event);
      };
    } else {
      console.log('dispatching events not supported in this browser');
    }
  });
})();
