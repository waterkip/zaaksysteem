// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.sysin', [
    'Zaaksysteem.sysin.links',
    'Zaaksysteem.sysin.transactions',
    'Zaaksysteem.sysin.records',
    'Zaaksysteem.sysin.datastore',
  ]);
})();
