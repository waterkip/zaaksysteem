// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('name', [
    '$interpolate',
    function ($interpolate) {
      return {
        restrict: 'A',
        priority: 1000,
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            if (
              $element[0].tagName &&
              $element[0].tagName.toLowerCase() !== 'comment'
            ) {
              $attrs.name = $interpolate($attrs.name)($scope);
              $element.attr('name', $attrs.name);
            }
          },
        ],
      };
    },
  ]);
})();
