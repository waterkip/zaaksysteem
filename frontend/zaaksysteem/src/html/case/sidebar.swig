<div class="dossier-sidebar" data-zs-accordion data-zs-accordion-active="<[!caseSidebar.getActions().length?'1':'0']>">
	<h3 class="ui-accordion-header">%%Acties%% (<[caseSidebar.getActions().length]>)</h3>
	<div class="case-actions" data-zs-case-action-list>
	    <div class="inner" data-ng-include="'/html/case/sidebar/actions.html'">
	    </div>
	</div>

	<h3 class="ui-accordion-header" data-ng-if="showChecklist">%%Checklist%% (<[caseSidebar.getChecklistItems().length]>)</h3>
	<div class="checklist" data-ng-if="showChecklist" data-zs-case-checklist>
	    <div class="inner" data-ng-include="'/html/case/sidebar/checklist.html'">
	    </div>
	</div>
</div>

<script type="text/zs-translation-data">
{
	"template": "%%Sjabloon%%",
	"email": "%%Email%%",
	"allocation": "%%Toewijzing%%",
	"case": "%%Zaak%%"
}
</script>

<script type="text/ng-template" id="/html/case/sidebar/actions.html">
	<ul data-ng-class="{ 'fase-afgerond': closed }">
		<li class="dossier-sidebar-item case-action" data-ng-class="{ 'action-disabled': isDisabled(action), 'case-action-tainted': action.tainted }" data-ng-repeat="action in caseActionList.getActions() track by action.id" data-ng-include="'/html/case/sidebar/action-item.html'" data-zs-case-action data-action-id="<[action.id]>">
		</li>
	</ul>
</script>

<script type="text/ng-template" id="/html/case/sidebar/action-item.html">
	<div class="case-action-info case-action-<[action.type]>" data-ng-include="'/html/case/sidebar/action-info-type-' + caseActionList.getActionInfoTemplateType(action) + '.html'">
	</div>
	<div class="case-action-state">
		<input type="checkbox" data-ng-disabled="caseActionList.isDisabled(action)" data-ng-model="action.automatic" data-ng-click="caseActionList.handleCheckboxClick($event)" data-ng-change="caseActionList.updateItem(action)" data-zs-title="<[(caseActionList.isDisabled(action)&&!caseActionList.isClosed())&&'%%In de laatste fase kan een deelzaak niet automatisch worden gestart.%%'||'%%Aangevinkte acties worden automatisch uitgevoerd bij het afronden van de fase%%']>"/>
	</div>
	<button data-ng-show="action.tainted" data-ng-click="caseActionList.resetAction($event, action)" class="mdi mdi-content-save" data-zs-title="%%Instellingen opgeslagen. Actie kan niet door regels worden beïnvloed. Klik om vrij te geven.%%" data-ng-disabled="caseActionList.isDisabled(action)"></button>
	<div data-zs-spinner="caseAction.isLoading()" class="zs-spinner-small zs-spinner-instant">
	</div>
</script>

<script type="text/ng-template" id="/html/case/action-type-template.html">
	[[ actioncontent('template') ]]
	<div class="case action-info case-action-template" data-zs-popup="'/html/case/action-form-template.html'" data-ng-click="!caseActionList.isClosed()&&openPopup()" data-zs-title="<[action.description]>">
	
	</div>
</script>

<script type="text/ng-template" id="/html/case/sidebar/action-info-type-template.html">
	<div class="case-action-info case-action-<[action.type]>"
		data-zs-popup="'/html/case/action-form-template.html'"
		data-ng-click="!caseActionList.isClosed()&&openPopup()"
		data-zs-title="<[action.description]>"
	>
		<div class="case-action-info-extended" data-ng-include="'/html/case/sidebar/action-info.html'">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/case/sidebar/action-info-type-email.html">
	<div class="case-action-info case-action-<[action.type]>"
		data-zs-popup="'/html/case/email-attachment-form.html'"
		data-ng-click="!caseActionList.isClosed()&&openPopup()"
		data-ng-init="attachments=[];context='actions'"
		data-zs-title="<[action.description]>"
	>
		<div class="case-action-info-extended" data-ng-include="'/html/case/sidebar/action-info.html'">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/case/sidebar/action-info-type-other.html">
	<div class="case-action-info case-action-<[action.type]>"
		href="<[action.url]>"
		title="<[action.description]>"
		data-zs-title="<[action.description]>"
		data-ng-class="{ 'ezra_dialog': !caseActionList.isClosed() }"
		rel="ezra_dialog_layout: large;"
	>
		<div class="case-action-info-extended" data-ng-include="'/html/case/sidebar/action-info.html'">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/case/sidebar/action-info.html">
	<div class="case-action-icon mdi"></div>
	<div class="case-action-label"><[action.label]></div>
	<br/>
	<span><[action.data.description]></span>
</script>

<script type="text/ng-template" id="/html/case/action-type-email.html">
	[[ actioncontent('email') ]]
</script>

<script type="text/ng-template" id="/html/case/action-type-other.html">
	[[ actioncontent('other') ]]
</script>

<script type="text/ng-template" id="/html/case/action-form.html">
	<div data-zs-modal data-ng-init="title=action.label">
		<div data-ng-include="action.url">
		</div>
	</div>
</script>

<script type="text/ng-template" id="/html/case/action-form-template.html">

	<div class="xential-modal" data-zs-modal data-ng-init="title=action.label">

		<div ng-controller="nl.mintlab.case.CaseTemplateController">

			<zs-case-template-form
				case-id="caseId"
				templates="[ template ]"
				show-template-select="false"
				case-docs="getCaseDocs()"
				data-actions="actions"
				data-values=" { template: template, name: filename, caseDocument: caseDocument, targetFormat: targetFormat }"
				ng-if="templates.length"
				on-done="closePopup()"
			>
			</zs-case-template-form>
		</div>

	</div>
</script>

<script type="text/ng-template" id="/html/case/sidebar/checklist.html">
	<ul>
	    <li data-ng-repeat="item in caseChecklist.getChecklistItems()" class="checklist-item dossier-sidebar-item <[item.user_defined&&'user-defined-checklist-item'||'']> <[item.id==-1&&'checklist-item-pending'||'']> <[item.checked&&'checklist-item-checked'||'']>">
	        <label>
	            <input type="checkbox" data-ng-model="item.checked" data-ng-disabled="caseChecklist.isClosed()" data-ng-change="caseChecklist.updateItem(item)"/>
            	<[item.label]>
            	<button data-ng-click="caseChecklist.removeItem(item)" data-ng-show="!caseChecklist.isClosed()&&item.user_defined" class="icon icon-del"
	            title="%%Verwijder taak%%"></button>
	        </label>
	    </li>
	</ul>
	<form data-ng-submit="caseChecklist.addItem(label);label=''" data-ng-show="!caseChecklist.isClosed()">
	    <input type="text" data-ng-model="label" data-ng-required="true" data-zs-placeholder="'%%Voeg hier een taak toe aan de checklist...%%'" />
	</form>
</script>
