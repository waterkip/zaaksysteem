#!/usr/bin/env bash

set -e

usage() {
    cat <<OEF
$(basename $0) OPTIONS

Build a docker perl base image and sets the correct tag in the Docker
files. Before pushing to the registery be sure to login :). Be advised
that you need to run this from the root of the git repository.

OPTIONS:

-f      Force a rebuild - without cache or tag checking
-p      Push the build to the registry
-t      Set a custom tag
-T      Skip tag checking
-r      Set the registry name
-s      Show the tags
-v      Verbose, show what we do

OEF
    exit
}

add_opts() {
    opts="$opts $*"
}

registry_name=registry.gitlab.com/zaaksysteem/zaaksysteem-perl

_docker_perl='docker/Dockerfile.perl'
_docker_backend='docker/Dockerfile.backend'
_cpan=cpanfile

opts=""
push=0
rebuild=0;

tag_check=1
show_tag=0

while getopts "fhpt:r:Tsv" name
do
    case $name in
        f) rebuild=1;;
        p) push=1;;
        t) tag=$OPTARG;;
        T) tag_check=0;;
        h) usage;;
        r) registry_name=$OPTARG;;
        s) show_tag=1;;
        v) set -x;;
    esac
done
shift $((OPTIND - 1))

for i in $_docker_perl $_docker_backend $_cpan
do
    if [ ! -f "$i" ];
    then

        echo "No $i file found" >&2
        exit 2;
    fi
done


# Multistage builds ftw we can now do more effective caching in a build step
tag_perl=$(grep '^FROM' $_docker_perl | head -1 | awk '{print $2}')
docker pull -q $tag_perl &>/dev/null
perl_sha=$(docker images -q --digests $tag_perl)

generated_tag=$(cat $_docker_perl $_cpan)
generated_tag="$(echo "$generated_tag" $perl_sha | shasum -a 256 | cut -b 1-7)"
tag=${tag:-$generated_tag}

show_tag() {
    echo "Current tag: $current_tag"
    echo "Building tag: $generated_tag"
}

do_push() {
    if [ $push -eq 1 ]
    then
        docker push $registry
        exit $?
    fi
    echo "Not pushing to the registry, supply -p option to do that"
}

registry=$registry_name:$tag

current_tag=$(grep "FROM" $_docker_backend |head -1 | grep $registry_name| \
    awk '{print $2}'| awk -F\: '{print $2}')

if [ $show_tag -eq 1 ]
then
    show_tag
    exit 0
fi

if [ $rebuild -eq 0 ] && [ $tag_check -eq 1 ] && [ "$generated_tag" = "$current_tag" ];
then
    echo "Tags ($tag) are equal, will not rebuild the image";
    do_push
    exit 2;
fi

if [ $rebuild -eq 1 ]
then
    add_opts --no-cache
fi

show_tag
docker build $ZS_DOCKER_OPTS $opts -f "$_docker_perl" -t $registry .

sed -i -e "s/$current_tag/$tag/" $_docker_backend

do_push

# vim: filetype=sh syntax=sh
