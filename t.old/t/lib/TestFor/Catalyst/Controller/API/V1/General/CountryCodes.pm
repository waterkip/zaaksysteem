package TestFor::Catalyst::Controller::API::V1::General::CountryCodes;
use base qw(ZSTest::Catalyst);

use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::General::CountryCodes - Test file for General::CountryCodes in our v1 API

=head1 SYNOPSIS

    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/General/CountryCodes.pm

=head1 DESCRIPTION

This module tests the C</api/v1/general/country_codes> namespace.

=cut


sub api_v1_general_legal_entity_types : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(
        sub {
            my $mech = $zs->mech;
            $mech->zs_login;
            my $base_url
                = $mech->zs_url_base . '/api/v1/general/country_codes';

            {
                my $data        = $mech->get_json_ok($base_url);
                my $type = {
                    dutch_code  => '9076',
                    label       => "Abessini\x{eb}",
                    alpha_one   => undef,
                    alpha_two   => undef,
                    alpha_three => undef,
                    code        => undef,
                };

                cmp_deeply($data->{result}{instance}{rows}[0]{instance}, $type, "Country is correct");
            }

            {
                my $data        = $mech->get_json_ok("$base_url/6030");
                my $type = {
                    dutch_code  => '6030',
                    label       => 'Nederland',
                    alpha_one   => undef,
                    alpha_two   => undef,
                    alpha_three => undef,
                    code        => undef,
                };

                cmp_deeply($data->{result}{instance}, $type, "Country code for NL is correct");
            }

            {
                $mech->get("$base_url/10001");
                $mech->validate_api_v1_error(
                    regexp => qr/Unable to find country code '10001'/,
                );
            }
        },
        'Get all the legal entity types',
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
