package TestFor::General::Actions::CanChange;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::General::Actions;

my @flash_messages;
my $permissions = {};


sub set_case_settings {
    my ($c, $stash, $case_settings) = @_;
    my $zaak = Test::MockObject->new;
    $stash->{zaak} = $zaak;
    $zaak->mock('is_afgehandeld', sub { $case_settings->{is_resolved} });

    if ($case_settings->{behandelaar_gm_id}) {
        my $behandelaar = Test::MockObject->new;
        $behandelaar->mock('gegevens_magazijn_id', sub { $case_settings->{behandelaar_gm_id} });
        $zaak->mock('behandelaar', sub { $behandelaar });
    } else {
        $zaak->mock('behandelaar', sub { undef });
    }

    if ($case_settings->{coordinator_gm_id}) {
        my $coordinator = Test::MockObject->new;
        $coordinator->mock('gegevens_magazijn_id', sub { $case_settings->{coordinator_gm_id} });
        $zaak->mock('coordinator', sub { $coordinator });
    } else {
        $zaak->mock('coordinator', sub { undef });
    }
}

sub set_user_settings {
    my ($c, $stash, $user_settings) = @_;

    if ($user_settings->{uidnumber}) {
        $c->mock('user_exists', sub { 1 });
    } else {
        $c->mock('user_exists', sub { 0 });
    }

    my $user = Test::MockObject->new;
    $user->mock('uidnumber', sub { $user_settings->{uidnumber} });
    $c->mock('user', sub { $user });

    $permissions->{zaak_beheer} = $user_settings->{zaak_beheer};
    $permissions->{zaak_edit} = $user_settings->{zaak_edit};
}


sub test_can_change {
    my ($case_settings, $user_settings) = @_;

    my $stash = {};
    my $c = Test::MockObject->new;
    $c->mock('stash', sub { $stash });

    my $req = Test::MockObject->new;
    $req->mock('param', sub { 1 });
    $c->mock('req', sub { $req });

    # reset for every test run
    @flash_messages = ();
    $c->mock('push_flash_message', sub {
        my ($c, @message) = @_;
        push @flash_messages, @message;
    });

    # reset for every test run
    $permissions->{zaak_beheer} = 0;
    $permissions->{zaak_edit} = 0;

    $c->mock('check_any_zaak_permission', sub {
        my ($c, @permissions) = @_;
        #diag "permissions: ", explain [@permissions];
        return grep { $permissions->{$_} } @permissions;
    });

    set_case_settings($c, $stash, $case_settings) if $case_settings;
    set_user_settings($c, $stash, $user_settings) if $user_settings;

    return Zaaksysteem::General::Actions::can_change($c);
}


sub can_change : Tests {

    ok !test_can_change,
        "No permission if no case supplied";

    ok !test_can_change({}, {}),
        "No permission if no user logged in";

    ok !test_can_change({is_resolved => 1}),
        "No permission if case resolved";

    ok test_can_change({}, {zaak_beheer => 1}),
        "Permission if case unresolved and user has zaak_beheer";

    ok test_can_change({behandelaar_gm_id => 1, coordinator_gm_id => 2}, {zaak_edit => 1}),
        "Permission if case unresolved and has behandelaar + coordinator and user has zaak_edit";

    ok test_can_change({behandelaar_gm_id => 23}, {uidnumber => 23}),
        "Permission if case unresolved and user is behandelaar of case";

    ok !test_can_change({behandelaar_gm_id => 23}, {uidnumber => 77}),
        "No permission if case unresolved and user is not behandelaar of case";

    ok test_can_change({behandelaar_gm_id => 12, coordinator_gm_id => 67}, {uidnumber => 67}),
        "Permission if case unresolved and user is coordinator of case";

    ok !test_can_change({coordinator_gm_id => 67}, {uidnumber => 67}),
        "No permission if case unresolved and has no behandelaar and user is coordinator of case";

    ok !test_can_change({coordinator_gm_id => 67}, {uidnumber => 77}),
        "No permission if case unresolved and user is not coordinator of case";

    ok !test_can_change({}, {uidnumber => 56}),
        "No permission if case unresolved and user has no specific rights";

    ok !test_can_change({}, {zaak_edit => 1}),
        "No permission if case unresolved and case has no behandelaar + coordinator and user has zaak_edit";

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

