package TestFor::General::ZAPI::Form::Action;
use base 'Test::Class';

use TestSetup;
use Zaaksysteem::ZAPI::Form::Action;

sub test_zapi_form_action : Tests {
    my $action1 = Zaaksysteem::ZAPI::Form::Action->new(
        name => 'foo',
    );

    is($action1->label,      'Foo',     'Default value of ->action is correct');
    is($action1->type,       'submit',  'Default value of ->type is correct');
    is($action1->importance, 'primary', 'Default value of ->importance is correct');
    is($action1->when,       undef,     '->when has no default value');
    is($action1->data,       undef,     '->data has no default value');

    is_deeply(
        $action1->TO_JSON,
        {
            name       => 'foo',
            label      => 'Foo',
            importance => 'primary',
            type       => 'submit',
            when       => undef,
            data       => undef,
        },
        'JSON representation of most basic action'
    );

    $action1->data({ bluh => 'blah' });
    is_deeply(
        $action1->TO_JSON,
        {
            name       => 'foo',
            label      => 'Foo',
            importance => 'primary',
            type       => 'submit',
            when       => undef,
            data       => { bluh => 'blah' },
            when       => undef
        },
        'JSON representation of most basic action + data'
    );

    $action1->when('forever');
    is_deeply(
        $action1->TO_JSON,
        {
            name       => 'foo',
            label      => 'Foo',
            importance => 'primary',
            type       => 'submit',
            when       => 'forever',
            data       => { bluh => 'blah' },
        },
        'JSON representation of most basic action + data + when'
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

