package TestFor::General::Object::Roles::Relation;

use base 'ZSTest';

use TestSetup;

use Test::DummyObjectRelatableA;
use Test::DummyObjectRelatableB;

sub zs_object_roles_relation : Tests {
    $zs->txn_ok(sub {
        my $a = Test::DummyObjectRelatableA->new;
        my $b = Test::DummyObjectRelatableB->new;

        use Zaaksysteem::Object::Model;
        {
            no warnings qw(redefine once);
            local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'Test::DummyObjectRelatableA' };

            my $model = Zaaksysteem::Object::Model->new(schema => $zs->schema);
            $b = $model->save_object(object => $b);

            local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'Test::DummyObjectRelatableB' };
            $a = $model->save_object(object => $a);
        }

        lives_ok {
            $a->relate($b);
        } 'object with relation role can relate';

        ok($a->is_deleteable, "Basic relationship keeps object deleteable");

        _relationship_ok($a, {
            relationship_name_a => 'related',
            relationship_name_b => 'related'
        });

        my %opts = (
            relationship_name_a => "Mothership",
            relationship_name_b => "Shuttlecraft",
            blocks_deletion     => 1,
        );

        lives_ok {
            $a->relate($b, %opts);
        } 'object with relation role can relate with options';

        ok(!$a->is_deleteable, "Relationship with 'block_deletion = 1' makes object undeleteable");

        _relationship_ok($a, \%opts);
    });
}

sub _relationship_ok {
    my ($obj, $want) = @_;

    my $relations = $obj->relations;
    is(@$relations, 1, "One relationship found");
    my $r = $relations->[-1];

    foreach (keys %$want) {
        is $r->$_, $want->{$_}, "$_ has the correct value";
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
