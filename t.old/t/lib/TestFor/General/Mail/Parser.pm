package TestFor::General::Mail::Parser;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::Mail::Parser;
use File::Spec::Functions qw(catfile);

sub zs_mail_parser : Tests {
    $zs->txn_ok(
        sub {

            parse_mail(catfile(qw(t inc Documents marco.mime)), 3);
            parse_mail(catfile(qw(t inc Documents mail_with_odd_chars.mime)), 1);

        }, "Mail parsing goes very very well",
    );
}

sub parse_mail {
    my ($mail, $max) = @_;
    my $parser = Zaaksysteem::Mail::Parser->new(schema => $zs->schema,);

    open my $fh, '<', $mail;
    my $content;
    { local $/; $content = <$fh>; }
    close($fh);

    my $files = $parser->parse_message($content);
    is(@$files, $max, "$max file found in mail");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
