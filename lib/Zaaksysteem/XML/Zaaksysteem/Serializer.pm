package Zaaksysteem::XML::Zaaksysteem::Serializer;
use Zaaksysteem::Moose;

use Encode;
use XML::LibXML;
use Zaaksysteem::XML::Compile;

=head1 ATTRIBUTES

=head2 schema

A Zaaksysteem DB schema

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 xential

The Xential SOAP implementation

=cut

has xml => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return Zaaksysteem::XML::Compile->xml_compile->add_class(
            'Zaaksysteem::XML::Zaaksysteem::Instance')->zaaksysteem;
    }
);

sub _generate_header {
    my $self = shift;
    return (
        Header => {
            Timestamp  => DateTime->now(),
            Identifier => $self->schema->customer_config->{instance_uuid},
        },
    );
}

=head1 METHODS

=head2 case_to_xml

Serializes a case to XML.
This functions makes use of object_data and transforms
that to XML.

=cut

sub _cdata_value {
    my ($self, $doc, $value) = @_;

    my $ref = ref $value;
    if ($ref eq 'HASH') {
        if ($value->{filename}) {
            $value = $value->{filename};
        }
        elsif ($value->{type} eq 'relationship') {
            $value = $value->{specifics}{metadata}{summary};
        }
        else {
            $self->log->debug(dump_terse($value));
            $value = "Unsupported attribute type for Xential"
        }
    }
    elsif ($ref) {
        $self->log->debug(dump_terse($value));
        $value = "Unsupported attribute type for Xential"
    }
    return $doc->createCDATASection($value);
}

sub case_to_xml {
    my $self = shift;
    my $case = shift;

    $case->discard_changes();

    if ($case->is_afgehandeld) {
        throw("XML/Serializer/Case/Nonexistent", 'You cannot serialize a closed case');
    }

    my $data =  $case->TO_JSON_V0;
    my $doc   = XML::LibXML::Document->new('1.0', 'UTF-8');

    my @attributes;
    foreach (keys %{$data->{values}}) {
        my $value = $data->{values}{$_};

        if ($_ eq 'case.confidentiality') {
            push(@attributes,
                {
                    magicstring => $_,
                    value       => $value->{mapped},
                }
            );
            next;
        }
        elsif ($_ eq 'case.deadline_timeline') {
            push(@attributes,
                {
                    magicstring => $_,
                }
            );
            next;
        }
        elsif ($_ eq 'case.current_deadline') {
            push(@attributes,
                {
                    magicstring => $_,
                }
            );
            next;
        }


        if (defined $value) {
            my $ref = ref $value;

            if ($ref eq 'ARRAY') {
                foreach (@$value) {
                    $_ = $self->_cdata_value($doc, $_);
                }
                push(@attributes,
                    {
                        magicstring => $_,
                        value => $value,
                    }
                );
                next;
            }
            elsif ($ref eq 'HASH' or !$ref && length($value)) {
                push(@attributes,
                    {
                        magicstring => $_,
                        value => $self->_cdata_value($doc, $value),
                    }
                );
                next;
            }
        }
        push(@attributes,
            {
                magicstring => $_,
            }
        );
    }

    return decode_utf8(
        $self->xml->case_attributes(
            'writer',
            {
                $self->_generate_header(),
                Case => {
                    UUID      => $data->{id},
                    Attribute => \@attributes,
                },
            }
        )
    );
}

=head2 catalogue_to_xml

Serializes the catalogue to XML.

=cut

sub catalogue_to_xml {
    my $self = shift;

    my $rs = $self->schema->resultset('BibliotheekKenmerken')->search({deleted => undef});

    my @attributes;
    while (my $r = $rs->next) {
        my @values;

        my $dvals = $r->bibliotheek_kenmerken_values->search_rs({ active => 1 },
            { order_by => { '-asc' => 'sort_order' } });

        while (my $d = $dvals->next) {
            push(@values, $d->value);
        }

        push(
            @attributes,
            {
                type        => $r->value_type,
                magicstring => $r->magic_string,
                label       => $r->label,
                description => $r->description,
                help        => $r->help,
                multiple    => $r->type_multiple,
                system      => $r->system,
                default     => \@values,
            }
        );
    }

    return decode_utf8(
        $self->xml->catalogue_attributes(
            'writer',
            { $self->_generate_header(), Attribute => \@attributes, }
        )
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
