use utf8;
package Zaaksysteem::Schema::CasetypeV1Reference;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CasetypeV1Reference

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<casetype_v1_reference>

=cut

__PACKAGE__->table("casetype_v1_reference");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 1

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 version

  data_type: 'integer'
  is_nullable: 1

=head2 casetype_node_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 1 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "version",
  { data_type => "integer", is_nullable => 1 },
  "casetype_node_id",
  { data_type => "integer", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-03-22 19:21:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fb19ucM/dndcA7rVeHNeHg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
