use utf8;
package Zaaksysteem::Schema::ViewCaseRelationshipV1Json;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ViewCaseRelationshipV1Json

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<view_case_relationship_v1_json>

=cut

__PACKAGE__->table("view_case_relationship_v1_json");
__PACKAGE__->result_source_instance->view_definition(" SELECT view_case_relationship.case_id,\n    view_case_relationship.type,\n    jsonb_agg(json_build_object('type', 'case', 'reference', view_case_relationship.relation_uuid) ORDER BY view_case_relationship.order_seq) AS relationship\n   FROM view_case_relationship\n  GROUP BY view_case_relationship.case_id, view_case_relationship.type");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 type

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 relationship

  data_type: 'jsonb'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "type",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "relationship",
  { data_type => "jsonb", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-02-07 13:48:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:aMXQ+sQo6ftmuGBNLD2BJA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
