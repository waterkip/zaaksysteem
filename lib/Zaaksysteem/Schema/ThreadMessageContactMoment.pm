use utf8;
package Zaaksysteem::Schema::ThreadMessageContactMoment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ThreadMessageContactMoment

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread_message_contact_moment>

=cut

__PACKAGE__->table("thread_message_contact_moment");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_message_contact_moment_id_seq'

=head2 content

  data_type: 'text'
  is_nullable: 0

=head2 contact_channel

  data_type: 'text'
  is_nullable: 0

=head2 direction

  data_type: 'text'
  is_nullable: 1

=head2 recipient_uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 recipient_displayname

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_message_contact_moment_id_seq",
  },
  "content",
  { data_type => "text", is_nullable => 0 },
  "contact_channel",
  { data_type => "text", is_nullable => 0 },
  "direction",
  { data_type => "text", is_nullable => 1 },
  "recipient_uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "recipient_displayname",
  { data_type => "text", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 thread_messages

Type: has_many

Related object: L<Zaaksysteem::Schema::ThreadMessage>

=cut

__PACKAGE__->has_many(
  "thread_messages",
  "Zaaksysteem::Schema::ThreadMessage",
  { "foreign.thread_message_contact_moment_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2019-08-29 09:48:53
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ZOfSEEJrUmEIvvsjPjHbDw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
