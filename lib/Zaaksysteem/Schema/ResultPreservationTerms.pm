use utf8;
package Zaaksysteem::Schema::ResultPreservationTerms;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ResultPreservationTerms

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<result_preservation_terms>

=cut

__PACKAGE__->table("result_preservation_terms");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'result_preservation_terms_id_seq'

=head2 code

  data_type: 'integer'
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 0

=head2 unit

  data_type: 'text'
  is_nullable: 0

=head2 unit_amount

  data_type: 'numeric'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "result_preservation_terms_id_seq",
  },
  "code",
  { data_type => "integer", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 0 },
  "unit",
  { data_type => "text", is_nullable => 0 },
  "unit_amount",
  { data_type => "numeric", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<result_preservation_terms_code_key>

=over 4

=item * L</code>

=back

=cut

__PACKAGE__->add_unique_constraint("result_preservation_terms_code_key", ["code"]);

=head2 C<result_preservation_terms_label_key>

=over 4

=item * L</label>

=back

=cut

__PACKAGE__->add_unique_constraint("result_preservation_terms_label_key", ["label"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-08-31 17:51:52
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8az+FeYiQo7grTHNSEoBaQ


__PACKAGE__->load_components(
  "+Zaaksysteem::DB::Component::ResultPreservationTerms",
   __PACKAGE__->load_components()
 );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
