package Zaaksysteem::Constants::Datastore;

use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Datastore - Constants for datastore thing

=head1 DESCRIPTION

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Datastore qw(:all);

=cut

use Exporter qw[import];

our @EXPORT_OK  = qw(DATASTORE_CLASSES);

use constant DATASTORE_CLASSES => [
    qw(
        NatuurlijkPersoon
        Organisatie
        BagLigplaats
        BagNummeraanduiding
        BagOpenbareruimte
        BagPand
        BagStandplaats
        BagVerblijfsobject
        BagWoonplaats
    )
];

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
