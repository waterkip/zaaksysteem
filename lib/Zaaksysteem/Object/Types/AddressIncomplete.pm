package Zaaksysteem::Object::Types::AddressIncomplete;
use Zaaksysteem::Moose;

extends 'Zaaksysteem::Object::Types::Address';
with qw(Zaaksysteem::Object::Roles::Relation);

=head1 NAME

Zaaksysteem::Object::Types::AddressIncomplete - Incomplete addresses in zaaksysteem

=head1 DESCRIPTION

An invalid object type for invalid addresses which we sometimes need to parse.

=head1 METHODS

=head2 check_object

    $self->check_object

Will run a assert_profile by checking if all necessary attributes are set. Think of zipcodes
when the country is dutch etc.

=cut

sub check_object {
    my $self = shift;
    return 1;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
