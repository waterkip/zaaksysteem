package Zaaksysteem::Object::ValueType::Datetime;

use Moose;

with 'Zaaksysteem::Interface::ValueType';

=head1 NAME

Zaaksysteem::Object::ValueType::Datetime - C<datetime> value instances
and behavior for L<Zaaksysteem::Object>s.

=head1 DESCRIPTION

=cut

use BTTW::Tools;

use DateTime;
use DateTime::Format::ISO8601;
use IO::Scalar;

=head1 METHODS

=head2 name

Implements type name constant interface for L<Zaaksysteem::Interface::ValueType>.

Returns the static string C<datetime>.

=cut

sub name {
    return 'datetime';
}

=head2 compare

Implements a generic comperator for C<datetime> values.

Returns whatever L<DateTime/compare> returns.

=cut

sub compare {
    my $self = shift;
    my $a = $self->as_datetime(shift);
    my $b = $self->as_datetime(shift);

    return unless defined $a && defined $b;

    return DateTime->compare($a, $b);
}

=head2 equal 

Implements equality testing of two values in C<datetime> context.

=cut

sub equal {
    return shift->compare(@_) == 0;
}

=head2 not_equal

Implements non-equality testing of two values in C<datetime> context.

=cut

sub not_equal {
    return shift->compare(@_) != 0;
}

=head2 less_than

Implements less-than testing of two values in C<datetime> context.

=cut

sub less_than {
    return shift->compare(@_) < 0;
}

=head2 greater_than

Implements greater-than testing of two values in C<datetime> context.

=cut

sub greater_than {
    return shift->compare(@_) > 0;
}

=head2 equal_or_less_than

Implements equal-or-less-than testing of two values in C<datetime> context.

=cut

sub equal_or_less_than {
    return shift->compare(@_) <= 0;
}

=head2 equal_or_greater_than

Implements equal-or-greater-than testing of two values in C<datetime> context.

=cut

sub equal_or_greater_than {
    return shift->compare(@_) >= 0;
}

=head2 as_datetime

Returns a L<DateTime> object derived from the provided
L<Zaaksysteem::Object::Value> object.

=cut

sub as_datetime {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;
    return $value->value if $value->type_name eq 'datetime';

    if ($value->type_name eq 'string' || $value->type_name eq 'text') {
        # eval because parse_datetime is no longer configurable to shut up
        return eval { DateTime::Format::ISO8601->parse_datetime($value->value) };
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
