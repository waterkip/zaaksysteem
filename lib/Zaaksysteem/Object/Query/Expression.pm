package Zaaksysteem::Object::Query::Expression;

use Moose::Role;

requires qw[
    stringify
];

=head1 NAME

Zaaksysteem::Object::Query::Expression - Query expression interface

=head1 DESCRIPTION

This role identifies object query expressions.

=head1 REQUIRED METHODS

=head2 stringify

This role requires an implementation of a method that stringifies, in a
human-readable format, the expression it's state represents.

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
