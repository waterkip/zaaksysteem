package Zaaksysteem::Object::Roles::Describe;

use Moose::Role;

requires qw[TO_JSON];

=head1 NAME

Zaaksysteem::Object::Roles::Describe - Augment L<Zaaksysteem::Object>
serialization behavior with attribute descriptions

=head1 DESCRIPTION

Apply this role to your object if you want to include the attribute
descriptions in the output of the L<TO_JSON|Zaaksysteem::Object/TO_JSON>.

By default all L<attribute instances|Zaaksysteem::Object/attribute_instances>
are described in a first-level key of the C<TO_JSON> output, called
C<describe>.

If you want a subset of all attribute instances to hydrate instead, you must
add the attribute names to the L</describe_attributes> attribute.

    ensure_all_roles($object, 'Zaaksysteem::Object::Roles::Describe');

    $object->describe_attributes([ 'attr1', 'attr2', 'etc' ]);

    my $hydration = $object->TO_JSON;

The above example will produce a hashref akin to the one below:

    {
        # Usual object hydration content goes here.
        describe => [
            { name => 'attr1', attribute_type => 'attr1_type' },
            { name => 'attr2', attribute_type => 'attr2_type' },
            { name => 'etc', attribute_type => 'etc_type' },
        ]
    }

=head1 ATTRIBUTES

=head2 describe_attributes

This attribute holds an arrayref with attribute names that should be included
in the hydration of objects this role is applied on.

=head3 Interfaces

=over 4

=item has_describe_attributes

This method is the named predicate for this attribute.

=item clear_describe_attributes

This method is the named clearer for this attribute.

=item all_describe_attributes

This method is a named handler for the 'elements' method provided by the
C<Array> trait.

=item add_describe_attribute

This method is a named handler for the 'push' method provided by the C<Array>
trait.

=back

=cut

has describe_attributes => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    predicate => 'has_describe_attributes',
    clearer => 'clear_describe_attributes',
    handles => {
        all_describe_attributes => 'elements',
        add_describe_attribute => 'push'
    }
);

=head1 METHODS

=head2 TO_JSON

This role wraps the L<Zaaksysteem::Object/TO_JSON> method to add an additional
key in the final hydration of the object.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig(@_);

    my @descriptions;

    # Unless explicitly defined, assume all attribute instances should be
    # described.
    if($self->has_describe_attributes) {
        push @descriptions, map { $_->_json_data }
                            map { $self->attribute_instance($_) }
                                $self->all_describe_attributes;
    } else {
        push @descriptions, map { $_->_json_data }
                                $self->attribute_instances;
    }

    $retval->{ describe } = \@descriptions;

    return $retval;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
