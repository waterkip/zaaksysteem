package Zaaksysteem::ZAPI::Response::Array;

use Data::Page;
use Moose::Role;

has 'pager' => (
    is      => 'rw',
);

sub from_array {
    my $self        = shift;
    my $array       = shift;

    die('Not a valid ARRAY: ' . ref($array))
        unless UNIVERSAL::isa($array, 'ARRAY');

    $self->_input_type('array');

    my $pager       = Data::Page->new();

    $pager->total_entries   (scalar(@{ $array }));

    if ($self->no_pager) {
        $pager->entries_per_page(scalar(@{ $array }) || 1);
    } else {
        $pager->entries_per_page($self->page_size);
    }

    $pager->current_page    ($self->page_current);

    $self->_generate_paging_attributes(
        $self->pager($pager),
    );

    $self->result([ $pager->splice($array) ]);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'ARRAY')
    ) {
        $self->from_array(@_);
    }

    $self->$orig( @_ );
};

around _validate_response => sub {
    my $method      = shift;
    my $self        = shift;

    if ($self->_input_type eq 'array') {
        die('Invalid format for result attribute')
            unless ref($self->result) eq 'ARRAY';
    }

    return $self->$method(@_);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 from_array

TODO: Fix the POD

=cut

