package Zaaksysteem::Backend::Sysin::Modules::ExportQueue;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Backend::Sysin::Modules';
with qw(
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
);


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::ExportQueue - ExportQueue integration module

=head1 DESCRIPTION

This module deals with the logic of informing third parties of when a export is
ready

=cut

use HTTP::Request::Common qw(POST);

use BTTW::Tools::UA qw(new_user_agent);
use BTTW::Tools;
use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Form::Field;

=head1 CONSTANTS

=head2 INTERFACE_CONFIG_FIELDS

Collection of L<Zaaksysteem::ZAPI::Form> objects used in building the UI for
this module.

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name  => 'interface_api_endpoint',
        type  => 'text',
        label => 'Endpoint triggerbericht',
        description =>
            'API Endpoint waarnaar het triggerbericht verstuurd wordt',
        required => 1,
        data     => { placeholder => 'https://api.example.com/export-queue' }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_medewerker',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description =>
            'Bepaal hier onder welke user de exportbestanden beschikbaar worden gemaakt.',
        data => {
            restrict    => 'contact/medewerker',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API sleutel',
        required    => 1,
        description => 'Interface key voor externe koppeling',
    ),
    client_certificate(
        required    => 1,
        label       => 'Client certificate (public)',
        description => 'Public key van het client certificaat',
    ),
    client_private_key(
        required    => 1,
        label       => 'Client certificate (private)',
        description => 'Private key van het client certificaat',
    ),
    ca_certificate(),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_tmlo',
        type => 'checkbox',
        label =>
            'Verstuur triggerbericht voor exports van overdrachtsbestanden',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name  => 'interface_search',
        type  => 'checkbox',
        label => 'Verstuur triggerbericht voor exports van zoekopdrachten',
    ),
];

=head2 INTERFACE_DESCRIPTION

Module description / pointers for the UI.

=cut

use constant INTERFACE_DESCRIPTION => <<'EOD';
<p>
Met dit koppelprofiel kan worden ingesteld dat exportbestanden aangeleverd
worden aan een derde partij vanuit een zoekopdracht export. Dit betekent dat
gebruikers een zoekopdracht kunnen exporteren, en dat een derde partij
automatisch ingelicht wordt indien de zoekopdracht klaar staat ter download.
</p>
EOD

=head2 MODULE_SETTINGS

Sysin module parameters, used during instantiation to configure the module.

=cut

use constant MODULE_SETTINGS => {
    name                          => 'export_queue',
    label                         => 'Systeemnotificatie na export',
    description                   => INTERFACE_DESCRIPTION,
    interface_config              => INTERFACE_CONFIG_FIELDS,
    is_multiple                   => 0,
    is_manual                     => 0,
    allow_multiple_configurations => 0,
    module_type                   => ['apiv1', 'api'],
    trigger_definition =>
        { send_trigger_message => { method => 'send_trigger_message' }, },

    interface_update_callback => sub {
        return shift->_update_interface_config(@_);
    },
};

sub _update_interface_config {
    my ($module, $interface, $model) = @_;

    my $config = $interface->get_interface_config;

    $module->get_certificate_info(
        $interface,
        $config,
        qw(ca_certificate client_certificate)
    );

    $interface->update_interface_config($config);
}

=head1 METHODS

=cut

around BUILDARGS => sub {
    my ($orig, $class) = @_;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 send_trigger_message

    $module->start_signing_procedure(
        {
            case_id       => 42,
            file_id       => 666,
            betrokkene_id => '...',
        },
        $interface
    );

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    my $model = $interface->model;
    $model->receive_event(%$params);

    $record->output(
        sprintf(
            "Received call back from external party:" . dump_terse($params)
        )
    );

    $transaction->preview_data({ preview_string => $record->output });

=cut

sub send_trigger_message {
    my ($self, $params, $interface) = @_;

    $interface->process(
        {
            processor_params => {
                processor => '_process_send_trigger_message',
                %$params,
            },
            direction               => 'outgoing',
            external_transaction_id => 'unknown',
            input_data =>
                JSON::XS->new->utf8(0)->canonical->pretty->encode($params),
        }
    );
}

sub _get_file {
    my ($interface, $name) = @_;

    my $schema = $interface->result_source->schema;
    my $rs     = $schema->resultset('Filestore');
    my $config = $interface->get_interface_config;
    my $fs     = $rs->find($config->{$name}[0]{id});
    return unless $fs;
    return $fs->get_path;
}

sub _process_send_trigger_message {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->log_params($params);

    my $ca          = _get_file($interface, 'ca_certificate');
    my $client_cert = _get_file($interface, 'client_certificate');
    my $client_key  = _get_file($interface, 'client_private_key');

    my $payload = $params->{payload};

    my $ua = new_user_agent(
        agent => 'Zaaksysteem',
        $ca ? (ca_file => "$ca") : (),
        $client_cert
        ? (
            client_cert => "$client_cert",
            client_key  => "$client_key"
            )
        : (),
    );

    my $req = POST(
        $interface->get_interface_config->{api_endpoint},
        Content      => JSON::encode_json($payload),
        Accept       => 'application/json',
        Content_Type => 'application/json',
    );
    $record->input($req->as_string);

    my $res;
    try {
        $res = $ua->request($req);
        if (!$res->is_success) {
            throw('sysin/module/export_queue/request', "Request failed");
        }
        $record->output($res->as_string);
    }
    catch {
        $self->log->info($_);

        $record->is_error(1);
        my @out = ($_);
        push(@out, $res ? $res->as_string : "No response from server");
        $record->output("Error: " . join("\n", @out));
    };
    return $record->output;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
