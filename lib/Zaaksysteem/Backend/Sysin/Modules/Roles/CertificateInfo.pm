package Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo;
use Moose::Role;

use Crypt::OpenSSL::X509;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo - Role for interfaces that need to verify a client certificate

=head1 SYNOPSIS

    package Zaaksysteem::Backend::Sysin::Modules::MyAwesomeInterface;
    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with qw/
        Zaaksysteem::Backend::Sysin::Modules::Roles::CertificateInfo
    /;


=head1 METHODS

=head2 get_certificate_info

Get all the information from a certificate that can be used to display
information

=cut

requires qw(_update_interface_config log);

sub get_certificate_info {
    my ($self, $interface, $config, @fields) = @_;

    my $filestore = $interface->result_source->schema->resultset('Filestore');

    foreach (@fields) {
        if ($config->{$_}) {
            if (@{$config->{$_}}) {
                my $cert = $config->{$_}[0];

                # Skip existing information
                next if $cert->{certificate_info};

                my $file = $filestore->find($cert->{id});

                # If we cannot fetch the file, continue silently
                next if !$file;

                $cert->{certificate_info} = $self->_get_certificate_info($file);
            }
            else {
                # Undef the thing instead of saving an array ref
                $config->{$_} = undef;
            }
        }
    }
}

sub _get_certificate_info {
    my ($self, $certificate) = @_;

    try {

        my $x509;
        if (ref $certificate) {
            $x509 = Crypt::OpenSSL::X509->new_from_file($certificate->get_path);
        }
        else {
            $x509 = Crypt::OpenSSL::X509->new_from_string($certificate);
        }

        my %info = (
            subject => $x509->subject,
            start   => $x509->notBefore,
            end     => $x509->notAfter,
            issuer  => $x509->issuer,
            version => $x509->version,
            serial  => $x509->serial,
        );

        my $m;
        foreach (qw(md5 sha1 sha224 sha256 sha384 sha512)) {
            $m = "fingerprint_$_";
            $info{$_} = $x509->$m;
        }

        return \%info;
    }
    catch {
        $self->log->info("Unable to retrieve certificate information: $_");
    };
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
