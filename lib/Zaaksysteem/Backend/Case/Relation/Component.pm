package Zaaksysteem::Backend::Case::Relation::Component;

use Moose;

BEGIN { extends 'DBIx::Class::Row'; }

sub TO_JSON {
    my $self = shift;

    return {
        left => $self->get_column('case_id_a'),
        right => $self->get_column('case_id_b'),
        seq_left => $self->order_seq_a,
        seq_right => $self->order_seq_b
    };
}

=head2 to_string

Returns a string representation of the relation, in the format C<[I<case_id_a> E<lt>-E<gt> I<case_id_b>]>.

=cut

sub to_string {
    my $self = shift;

    return sprintf(
        '[%d <-> %d]',
        $self->get_column('case_id_a'),
        $self->get_column('case_id_b')
    );
}

=head2 cases

Returns a list of case objects in this relation.

=cut

sub cases {
    my $self = shift;

    return $self->case_id_a, $self->case_id_b;
}

=head2 view_in_context

Returns a L<Zaaksysteem::Zaken::RelationView> in the context of a case. This means operations on the relation will be done B<ON THE OTHER CASE ID IN THE RELATION>.

=head3 Parameters

=over 4

=item case_id

The I<case_id> of the case to base the context of the view on

=back

=cut

sub view_in_context {
    Zaaksysteem::Zaken::RelationView->new(shift, shift);
}

=head2 delete

Wrapper around DBIx::Class::Row's delete method, inject events in the logging table automagically.

Also clears "vervolg_van" in the appropriate case if necessary.

=cut

after delete => sub {
    my $self = shift;

    if ($self->type_a eq 'continuation') {
        $self->case_id_a->update({ vervolg_van => undef });
    } elsif ($self->type_b eq 'continuation') {
        $self->case_id_b->update({ vervolg_van => undef });
    }

    for my $case ($self->cases) {
        $case->logging->trigger('case/update/relation', { component => 'zaak', data => {
            case_id => $case->id,
            relation_id => $self->view_in_context($case->id)->case_id,
            deleted => 1
        }});
    }
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 TO_JSON

TODO: Fix the POD

=cut

