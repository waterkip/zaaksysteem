package Zaaksysteem::Backend::Tools::WorkingDays;

use strict;
use warnings;

use DateTime;

use BTTW::Tools;

use Exporter 'import';
our @EXPORT_OK = qw/add_working_days diff_working_days/;

use Date::Holidays::NL;

=head2 add_working_days

Calculate the next working day given a start date and interval.
E.g. it 23 november 2014 now, and in 30 working days the case
must be closed - which date is the deadline?

=cut

define_profile add_working_days => (
    required => [qw[datetime working_days]],
    constraint_methods => {
        working_days => qr/^\-?\d+$/
    }
);

sub add_working_days {
    my $arguments = assert_profile(shift)->valid;

    my $next_day     = $arguments->{datetime}->clone;
    my $working_days = $arguments->{working_days};

    my $offset       = $working_days > 0 ? 1 : -1;

    while ($working_days != 0) {

        do { $next_day = $next_day->add(days => $offset) }
            while is_government_day_off($next_day);

        $working_days -= $offset;
    }

    return $next_day;
}

=head2 diff_working_days

Returns the difference (in working days) between two DateTime instances.

=cut

define_profile diff_working_days => (
    required => {
        date1 => 'DateTime',
        date2 => 'DateTime',
    },
);

sub diff_working_days {
    my $arguments = assert_profile(shift)->valid;

    my @dates = ($arguments->{date1}->clone, $arguments->{date2}->clone);

    my $factor = 1;
    if ($dates[0] > $dates[1]) {
        $factor = -1;
        unshift(@dates, pop(@dates));
    }

    my $delta = 0;
    while ($dates[0] < $dates[1]) {
        do { $dates[0] = $dates[0]->add(days => 1) }
            while is_government_day_off($dates[0]);

        $delta += 1;
    }

    # This only happens if $dates[1] happens to be during weekend. Don't count
    # the following Monday as one of the delta days.
    if($dates[0] != $dates[1]) {
        $delta -= 1;
    }

    return $delta * $factor;
}

=head2 is_government_day_off

Returns true if the DateTime specified is a Dutch holiday or weekend day.

=cut

sub is_government_day_off {
    my ($datetime) = @_;

    # day_of_week:
    # Returns the day of the week as a number, from 1..7,
    # with 1 being Monday and 7 being Sunday.
    return $datetime->day_of_week > 5 || is_holiday_dt($datetime, gov => 1);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
