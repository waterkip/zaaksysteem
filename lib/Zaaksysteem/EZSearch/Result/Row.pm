package Zaaksysteem::EZSearch::Result::Row;

use Moose;
use Zaaksysteem::Types qw/UUID/;

=head1 NAME

Zaaksysteem::EZSearch::Result::Row - A row containing a search result

=head1 DESCRIPTION

This is the primary result for a "search hit"

=head1 ATTRIBUTES

=head2 reference

Type: UUID

=cut

has 'reference' => (
    is          => 'rw',
    isa         => UUID,
    required    => 1,
);

=head2 search_type

Type: Str

The type of this hit, e.g. where to go for more information about the hit

=cut

has 'search_type' => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

=head2 score

Type: Int

The score of this hit as a number, the higher the better

=cut

has 'score' => (
    is          => 'rw',
    isa         => 'Int',
    lazy        => 1,
    default     => 1,
);

=head2 label

Type: Str

The label of this hit, to show to the user

=cut

has 'label' => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

=head2 description

Type: Str

The description of this object, like address data for a person.

=cut

has 'description' => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
