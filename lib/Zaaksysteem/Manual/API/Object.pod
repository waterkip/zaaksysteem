=head1 NAME

Zaaksysteem::Manual::API::Public - Zaaksysteem Public API description

=head1 DESCRIPTION

A description about how to use our public API. This document describes the whole road
from authenticating to actually getting some results.

Our public API is currently able to read from Zaaksysteem, but is not yet ready for mutations.

=head1 AUTHENTICATING

Authentication against our interface is possible by using our standard "HTTP Digest"
authentication method.

Before being able to use the HTTP Digest authentication method, you will have to create
an "Extern Koppelprofiel" via our "Koppelingen" interface within the Zaaksysteem dashboard.

You will be able to select an object type to work with, set an API key for authentication, and
will be able to impersonate a selected user.

You will need to use the API key as a password and the username of the selected user.

Example curl command:

    curl -k --digest -u "api:ApIKeyFromZaaksysteem" https://localhost/api/public/1/product

=head1 API

Below are the different calls you will be able to call via our public API.

=head2 OBJECT SEARCH

Url: C</api/public/[API_ID]/[OBJECT_TYPE]>

    curl -k --digest -u "api:ApIKeyFromZaaksysteem" https://localhost/api/public/44/product

=begin javascript

    {
       "next" : null,
       "prev" : null,
       "status_code" : "200",
       "num_rows" : "4",
       "rows" : 4,
       "comment" : null,
       "at" : null,
       "result" : [
          {
             "actions" : null,
             "object_type" : "product",
             "id" : "14ecb43f-6006-46ab-b003-449fd299e0c4",
             "object_id" : null,
             "values" : {
                "internal_explanation" : "",
                "search_terms" : null,
                "name" : "Woningzoekende, urgentieverklaring",
                "terms" : "<p>U kunt alleen een urgentiebewijs aanvragen als u:</p> <ul> <li>18 jaar of ouder bent</li> <li>staat ingeschreven in de&nbsp;basisregistratie personen (BRP)&nbsp;&nbsp;</li> <li>Nederlander bent&nbsp;</li> <li>geen gevangenisstraf, voorarrest, preventieve hechtenis, TBS en schulden heeft</li> <li>ingeschreven staat als woningzoekende&nbsp;</li> <li>niet in een woning zit voor tijdelijke huur</li> </ul>",
                "related_casetypes" : [],
                "description" : "<p>U kunt een urgentieverklaring aanvragen, als u dringend op zoek bent naar een huurwoning. Deze verklaring is er alleen voor noodgevallen.</p> <p>Wilt u meer informatie over het zoeken van een sociale huurwoning? Kijk dan bij woonzoekende, inschrijven.</p>",
                "category_id" : 2,
                "target" : [
                   "internal"
                ],
                "external_id" : 1660,
                "price" : "",
                "approach" : "<p>U levert bij de aanvraag onder andere:</p> <ul> <li>een inschrijvingsbewijs dat aantoont dat u op zoek bent naar een woning</li> <li>een schriftelijke verklaring waarom u de aanvraag doet (bijvoorbeeld een verklaring van arts of psycholoog)</li> </ul>",
                "update_on_import" : true
             }
          },
          {
            "actions" : null,
            // etc etc
          }

       ]
    }

=end javascript


When defining your "Extern Koppelprofiel", you will be able to select a list of object types you
can question. E.g. "product" or "vraag", or even "case". In the near future, you will be able
to use predefined search queries. This way you can define what output will be generated.

The response is a basic L<Zaaksysteem::Manual::API#JSON_RESPONSE>, containing the results of
this query.

B<Parameters>

=over 4

=item API_ID

The API_ID, which can be found on our "Koppelingen" page, besides the label of your created
"Extern Koppelprofiel".

=item OBJECT_TYPE

One of the defined object types in your "Extern Koppelprofiel".
Currently supported object types are 'product' and 'question', if they are enabled in the interface.

=back

=head1 SEE ALSO

L<Zaaksysteem::Manual::API> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
