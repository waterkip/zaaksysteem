=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Config - Type definition for config objects

=head1 DESCRIPTION

This page documents the serialization of C<config> objects.

=head1 JSON

=begin javascript

{
    "type": "config",
    "reference": "2a6a0ac0-d757-42e9-8dd9-0e4cf3c5bf0f",
    "instance": {
        "parameter": "my_param_name",
        "value": "my param value"
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 parameter E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Name of the config item

=head2 value E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Value of the config item

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
