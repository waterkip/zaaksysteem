package Zaaksysteem::Controller::API::v1::Sysin;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Sysin - API v1 controller for sysin Zaaksysteem information and calls

=head1 DESCRIPTION

This is the controller API class for C<api/v1/sysin>.

=cut

use BTTW::Tools;

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/sysin> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('sysin') : CaptureArgs(0) {}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
