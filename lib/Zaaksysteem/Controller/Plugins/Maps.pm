package Zaaksysteem::Controller::Plugins::Maps;
use Moose;

use BTTW::Tools;
use Zaaksysteem::Geo::BAG::Model;

BEGIN { extends 'Zaaksysteem::Controller' }

sub read : Chained('/'): PathPart('plugins/maps'): Args(0) {
    my ($self, $c) = @_;

    my $bag_model = Zaaksysteem::Geo::BAG::Model->new(schema => $c->model('DB')->schema);
    my $json_data = { success => 0, addresses => [] };

    if (my $term = $c->req->params->{term}) {
        my $addresses;
        if (my $parsed_term = $bag_model->parse_search_term($term)) {
            $addresses = [
                $bag_model->get_exact(%$parsed_term)
            ];
        } else {
            # Full text address search, get suggestions
            $addresses = $bag_model->search(
                type  => 'nummeraanduiding',
                query => $term,
            );
        }
        $json_data = {
            success => 1,
            addresses => [map { $_->to_maps_result } @$addresses],
        };
    } elsif ($c->req->params->{lat} && $c->req->params->{lon}) {
        my $lat = $c->req->params->{lat};
        my $lon = $c->req->params->{lon};

        my $nearest = _find_nearest($c, $lat, $lon);

        if ($nearest) {
            $json_data = {
                success => 1,
                # Use the "old" _to_maps_result, that works for our ElasticSearch BAG objects
                addresses => [_to_maps_result($nearest)]
            };
        } else {
            $json_data = {
                success => 0,
                message => "No objects found in range of '$lat,$lon'",
            };
        }
    }

    $c->stash->{json} = $json_data;
    $c->detach('View::JSONlegacy');
}

my $NEAREST_OFFSET       = "1m";
my $NEAREST_SCALE        = "50m";
my $NEAREST_MAX_DISTANCE = "500m";

sub _find_nearest {
    my ($c, $lat, $long) = @_;

    my $es = $c->model('Elasticsearch', { cluster => 'BAG' });

    my $query = {
        "size" => 1,
        "query" => {
            "bool" => {
                "must" => {
                    "function_score" => {
                        "gauss" => {
                            "geo_lat_lon" => {
                                "origin" => {
                                    "lat" => $lat,
                                    "lon" => $long,
                                },
                                "offset" => $NEAREST_OFFSET,
                                "scale"  => $NEAREST_SCALE,
                            }
                        }
                    }
                },
                "filter" => {
                    "geo_distance" => {
                        "distance" => $NEAREST_MAX_DISTANCE,
                        "geo_lat_lon" => {
                            "lat" => $lat,
                            "lon" => $long,
                        }
                    }
                }
            }
        }
    };

    my $results = try {
        $es->search(
            index => 'bag_nummeraanduiding',
            body  => $query,
        );
    } catch {
        $c->log->error("Error searching for BAG in Elasticsearch: '$_'");
        return;
    };

    return unless $results;
    return $results->{hits}{hits}[0]{_source};
}

sub _to_maps_result {
    my ($bag_data) = @_;

    my ($latitude, $longitude) = split(/,/, $bag_data->{geo_lat_lon});

    my $huisnummer = _build_huisnummer($bag_data->{woonplaats}{openbareruimte}{nummeraanduiding});

    my $address = sprintf(
        "%s %s, %s %s, %s",
        $bag_data->{straatnaam},
        $huisnummer,
        $bag_data->{postcode},
        $bag_data->{plaats},
        $bag_data->{land}
    );

    return {
       country  => $bag_data->{land},
       city     => $bag_data->{plaats},
       street   => $bag_data->{straatnaam},
       zipcode  => $bag_data->{postcode},
       province => $bag_data->{provincie},
       coordinates => {
           lat => $latitude,
           lng => $longitude,
       },
       identification => $address,
       address => $address,
    }
}

sub _build_huisnummer {
    my $nummeraanduiding = shift;

    my $huisnummer = $nummeraanduiding->{huisnummer};
    my $huisletter = $nummeraanduiding->{huisletter};
    my $huisnummer_toevoeging = $nummeraanduiding->{huisnummer_toevoeging};

    my $hn = $huisnummer;
    $hn .= $huisletter
        if defined $huisletter && length($huisletter);
    $hn .= "-$huisnummer_toevoeging"
        if defined $huisnummer_toevoeging && length($huisnummer_toevoeging);

    return $hn;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
