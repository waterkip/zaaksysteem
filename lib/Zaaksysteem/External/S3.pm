package Zaaksysteem::External::S3;
use strict;
use warnings;
use utf8;
use v5.30;

use BTTW::Tools qw(dump_terse);
use BTTW::Tools::UA qw(new_user_agent);
use Log::Log4perl;
use Net::Amazon::S3;
use Net::Amazon::S3::Authorization::Basic;
use Net::Amazon::S3::Authorization::IAM;
use Net::Amazon::S3::Vendor::Generic;
use Net::Amazon::S3::Signature::V2;
use Net::Amazon::S3::Signature::V4;
use Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity;

my $log;

sub build_s3 {
    my ($class, %args) = @_;

    $log //= Log::Log4perl->get_logger(__PACKAGE__);

    my %s3_args = (
        vendor => Net::Amazon::S3::Vendor::Generic->new (
            host => $args{host},
            use_https => $args{secure} // 1,
            use_virtual_host => $args{use_virtual_host},
            authorization_method => $args{authorization_method} // 'Net::Amazon::S3::Signature::V2',
            default_region => $args{region} // 'eu-central-1',
        ),
    );

    if ($args{use_iam_role}) {
        if ($ENV{AWS_WEB_IDENTITY_TOKEN_FILE}) {
            $s3_args{authorization_context} = Zaaksysteem::Filestore::Engine::S3::AssumeRoleWebIdentity->new(
                sts_region => $args{sts_region},
            );
        } else {
            $s3_args{authorization_context} = Net::Amazon::S3::Authorization::IAM->new();
        }
    } else {
        $s3_args{authorization_context} = Net::Amazon::S3::Authorization::Basic->new(
            aws_access_key_id     => $args{access_key},
            aws_secret_access_key => $args{secret_key},
        );
    }

    $log->trace("Configuring S3 with values: " . dump_terse(\%s3_args)) if $log->is_trace;

    my $s3 = Net::Amazon::S3->new(%s3_args);
    $s3->ua(new_user_agent());

    return $s3;
}

sub transform_download_url {
    my ($class, $url) = @_;

    $log //= Log::Log4perl->get_logger(__PACKAGE__);

    if ($url =~ m{^(https?)://([^/]+)/(.*)$}) {
        return "/download/s3/$1/$2/$3";
    }

    $log->warn("Presigned S3 URL not formatted as expected: '$url'");
    throw(
        "s3/transform_download_url",
        "Download URL doesn't match expected format.",
        { http_code => 500 },
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

aaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
