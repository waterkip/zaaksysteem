package Zaaksysteem::Test;
use strict;
use warnings;
use namespace::autoclean ();

use Import::Into;
use Test::Class::Moose ();
use Test::Fatal;
use Zaaksysteem::Test::Mocks ();
use Zaaksysteem::Test::Util ();
use Test::Pod::Coverage;

sub import {

    my $caller_level = 1;

    # Test::Class::Moose imports Test::Most, Test::Most imports *ALL*
    # functions of Test::Deep, Test::Deep has any, all, none, and some
    # others that List::Utils also has. We therefore ask
    # Test::Class::Moose not to import Test::Most and we then ask
    # Test::Most not to include some functions we want from List::Util
    # Test::Deep has EXPORT_TAGS but they include pretty much everything
    my @TEST_DEEP_LIST_UTILS = qw(!any !all !none);
    Test::Class::Moose->import::into($caller_level, bare => 1);
    Test::Most->import::into($caller_level, @TEST_DEEP_LIST_UTILS);

    my @imports = qw(
        namespace::autoclean
        Test::Fatal

        Zaaksysteem::Test::Mocks
        Zaaksysteem::Test::Util
        Test::Pod::Coverage
    );

    $_->import::into($caller_level) for @imports;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
