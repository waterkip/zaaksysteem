package Zaaksysteem::Test::Transaction;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Transaction - Tests for Zaaksysteem::Transaction

=head1 DESCRIPTION

Test the L<Zaaksysteem::Transaction> model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Transaction

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Test::Mocks;
use Zaaksysteem::Transaction;

sub test_cleanup_old_transactions {
    my $deletes_seen = 0;
    my $fake_schema = mock_one(
        txn_do => sub {
            my $arg = shift;
            return $arg->();
        },
        resultset => sub {
            my $arg = shift;

            if ($arg eq 'Transaction') {
                return mock_one(
                    search => sub {
                        my $search = shift;
                        my $opts = shift;
                        cmp_deeply(
                            $search,
                            {
                                '-or' => [
                                    {
                                        'date_created::DATE' => { '<' => \"(CURRENT_DATE - '120 days'::INTERVAL)"
                                        }
                                    },
                                    { 'date_deleted' => { '!=' => undef } }
                                ]
                            },
                            'Transaction search called with proper interval'
                        );
                        cmp_deeply(
                            $opts,
                            { rows => 50, columns => 'id' },
                            'Transaction search called with proper options'
                        );

                        return mock_one(
                            delete => sub {
                                $deletes_seen += 1;
                                pass("->delete called on transaction resultset");
                            },
                            count => '1',
                        )
                    },
                );
            }
            else {
                fail("Invalid resultset requested: '$arg'");
            }
        },
    );

    my $model = Zaaksysteem::Transaction->new(
        schema  => $fake_schema,
        enabled => 1
    );

    $model->cleanup_old_transactions();
    is($deletes_seen, 1, "1 ->delete calls were seen");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
