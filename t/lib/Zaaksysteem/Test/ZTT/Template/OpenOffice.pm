package Zaaksysteem::Test::ZTT::Template::OpenOffice;

=head1 NAME

Zaaksysteem::Test::ZTT::Template::OpenOffice - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::ZTT::Template::OpenOffice;

=cut

use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::ZTT::Template::OpenOffice;

use Sub::Override;
use Test::MockObject;

=head1 METHODS

=cut

=head2 test_iterate_table

=cut

sub test_iterate_table {
    my $self;

    my $TRACE = [];

    my $override = Sub::Override->new(
        'Zaaksysteem::ZTT::Template::OpenOffice::odfManifest'
            => \&_mock_obj__OpenOffice_OODoc_Manifest
    );

    lives_ok(
        sub {
            $self = Zaaksysteem::ZTT::Template::OpenOffice->new(
                document => _mock_obj__OpenOffice_OODoc_Document(),
                styles   => _mock_obj__OpenOffice_OODoc_Styles(),
#               style_paragraph_selectors # deliberatly use the defaults
            );
        },
        "can call method 'new' on 'Zaaksysteem::ZTT::Template::OpenOffice'"
    );

    isa_ok($self, 'Zaaksysteem::ZTT::Template::OpenOffice');
    
    cmp_deeply(
        $self->style_paragraph_selectors => [
            "/office:master-styles/style:master-page/style:header/text:p",
            "/office:master-styles/style:master-page/style:footer/text:p",
        ],
        "... and default have headers & footers"
    );

    my $mock_mod__Zaaksysteem_ZTT = _mock_mod__Zaaksysteem_ZTT( $TRACE );

    my $mocked_ztt = Zaaksysteem::ZTT->new();

    my $contexts = [ qw/foo bar qux/ ];

    my $mocked_selection = _mock_obj__Zaaksysteem_ZTT_Selection_OpenOffice(
        _mock_obj__OpenOffice_OODoc_Element( $TRACE )
    );

    note "Call 'iterate_table'";

    $self->iterate_table(
        $mocked_ztt,
        $contexts,
        $mocked_selection,
    );

    note "Check trace ... head, body, tail";
    
    my @TRACE_head = ( shift @$TRACE, shift @$TRACE );

    my @TRACE_tail = reverse ( pop @$TRACE, pop @$TRACE, pop @$TRACE );

    note "Check first";

    cmp_deeply(
        \@TRACE_head => [
             "OpenOffice::OODoc::Element -> child",
             [
                 ignore,
                 0,
                 "table:table-row",
             ]
        ],
        "Did call 'child' element with correct params"
    );

    note "Check iterations"; # well, only one

    is( $TRACE->[0],  "Zaaksysteem::ZTT -> cache",
        "Did call 'cache', probably for reusing"
    );
    is( $TRACE->[1],  "Zaaksysteem::ZTT -> add_context",
        "Did call 'add_context'"
    );
    cmp_deeply(
        $TRACE->[2] => [ 'foo' ],
        "... with the right context"
    );
    is( $TRACE->[3],  "Zaaksysteem::ZTT -> process_template",
        "Did call 'process_template'"
    );
    cmp_deeply(
        $TRACE->[4] => [ obj_isa('Zaaksysteem::ZTT::Template::OpenOffice') ],
        "... with a (newly created) 'Zaaksysteem::ZTT::Template::OpenOffice' object"
    );
    cmp_deeply(
        $TRACE->[4]->[0]->style_paragraph_selectors => [],
        "... and has empty 'style_paragraph_selectors'"
    ); # this is all we wanted to know, but hey, you've now some bonus tests

    note "Check finally done";

    cmp_deeply(
        \@TRACE_tail => [
             "OpenOffice::OODoc::Element -> delete",
             "OpenOffice::OODoc::Element -> setAttribute",
             [
                 ignore,
                 "table:name",
                 "iterdone",
             ]
        ],
        "Deletes temp element and sets name of table to 'iterdone'"
    );
}

sub _mock_obj__OpenOffice_OODoc_Document {
    my $TRACE = shift;

    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('OpenOffice::OODoc::Document');

    $mock_obj->mock(
        'insertTableRow' => sub {
            push @$TRACE, "OpenOffice::OODoc::Document -> insertTableRow";
            push @$TRACE, [ @_ ];
            return _mock_obj__OpenOffice_OODoc_Element($TRACE);
        }
    );

    return $mock_obj
}

sub _mock_obj__OpenOffice_OODoc_Element {
    my $TRACE = shift;

    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('OpenOffice::OODoc::Element');

    $mock_obj->mock(
        'child' => sub {
            push @$TRACE, "OpenOffice::OODoc::Element -> child";
            push @$TRACE, [ @_ ];
            return _mock_obj__OpenOffice_OODoc_Element($TRACE);
        }
    );

    $mock_obj->mock(
        'delete' => sub {
            push @$TRACE, "OpenOffice::OODoc::Element -> delete";
        }
    );

    $mock_obj->mock(
        'setAttribute' => sub {
            push @$TRACE, "OpenOffice::OODoc::Element -> setAttribute";
            push @$TRACE, [ @_ ];
        }
    );

    return $mock_obj
}

sub _mock_obj__OpenOffice_OODoc_Manifest {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('OpenOffice::OODoc::Manifest');

    return $mock_obj
}

sub _mock_obj__OpenOffice_OODoc_Styles {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('OpenOffice::OODoc::Styles');

    return $mock_obj
}

sub _mock_mod__Zaaksysteem_ZTT {
    my $TRACE = shift;
    my $mock_mod = Test::MockModule->new('Zaaksysteem::ZTT');

    $mock_mod->mock(
        'cache' => sub {
            shift;
            push @$TRACE, "Zaaksysteem::ZTT -> cache";
            return {};
        }
    );

    $mock_mod->mock(
        'add_context' => sub {
            shift;
            push @$TRACE, "Zaaksysteem::ZTT -> add_context";
            push @$TRACE, [ @_ ];
        }
    );

    $mock_mod->mock(
        'process_template' => sub {
            shift;
            push @$TRACE, "Zaaksysteem::ZTT -> process_template";
            push @$TRACE, [ @_ ];
        }
    );

    return $mock_mod
}

sub _mock_obj__Zaaksysteem_ZTT_Selection_OpenOffice {
    my $element = shift;
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::ZTT::Selection::OpenOffice');

    $mock_obj->mock(
        'element' => sub {
            return $element
        }
    );

    return $mock_obj
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
