package Zaaksysteem::Test::Object::Model;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Object::Model - Tests for the Object model

=head1 DESCRIPTION

Test the Zaaksysteem "Object" model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Model

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Object::Model;

sub test_acl_rs {
    my $last_capability;
    my $mock_model = mock_one(
        new_resultset => sub {
            return mock_one(
                search_rs => sub {
                    return { search_rs => [@_] };
                },
                acl_items => sub {
                    $last_capability = shift;
                    return ('a', 'b', 'c');
                },
            )
        }
    );

    my $result = Zaaksysteem::Object::Model::acl_rs($mock_model, 'frobnicate');
    cmp_deeply(
        $result,
        {
            search_rs => [
                { '-or' => [ 'a', 'b', 'c'] }
            ]
        },
        'acl_rs returns a new resultset with ACL items applied'
    );
    is($last_capability, 'frobnicate', 'Requested capability was passed to $rs->acl_items() correctly');
}

sub test_search_by_capability {
    my $mock_model = mock_one(
        acl_rs => sub {
            my $capability = shift;
            return "acl_rs for capability $capability";
        },
        _search_rs => sub {
            my $rs = shift;
            my @args = @_;

            return mock_one(
                rs => sub {
                    return {
                        acl_rs => $rs,
                        args => \@args,
                    };
                }
            );
        }
    );

    my $res = Zaaksysteem::Object::Model::search_by_capability(
        $mock_model,
        'some_capability',
        'some_type',
        { query => 'here' },
    );

    cmp_deeply(
        $res,
        {
            'acl_rs' => 'acl_rs for capability some_capability',
            'args'   => [
                'some_type',
                { 'query' => 'here' }
            ],
        },
        'search_by_capability calls acl_rs and _search_rs correctly'
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
