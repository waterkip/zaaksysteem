package Zaaksysteem::Test::Zaken::Betrokkenen;
use Moose;
extends "Zaaksysteem::Test::Moose";

=head1 NAME

Zaaksysteem::Test::Zaken::Betrokkenen - Test Zaaksysteem::Zaken::Betrokkenen

=head1 DESCRIPTION

Tests for the old "betrokkenen" handling on the database "zaak" object.

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Zaken::Betrokkenen

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Zaken::Betrokkenen;

sub test_set_betrokkene {
    my $mock = Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak->new();

    {
        my $override1 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_get_handle',
            sub {
                my $handle = mock_one(
                    get_handle_called => 1,
                    arguments => \@_,
                );
                return $handle;
            },
        );

        my $opts = {};
        my $rv1 = $mock->betrokkene_set(
            $opts,
            'aanvrager',
            { betrokkene => '31337' },
        );

        is(
            $rv1->{get_handle_called},
            1,
            'set_betrokkene with betrokkene in betrokkene_info -> _betrokkene_get_handle'
        );
        cmp_deeply(
            $rv1->{arguments},
            [ $mock, { betrokkene => 31337 } ],
            '_betrokkene_get_handle called correctly'
        );
        is_deeply(
            $opts->{aanvrager_gm_id},
            31337,
            'Requestor GM id returned correctly'
        );
    }

    {
        my $override1 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_get_handle',
            sub { die ":(" },
        );

        my $opts = {};

        throws_ok(
            sub {
                $mock->betrokkene_set(
                    $opts,
                    'aanvrager',
                    { betrokkene => '31337' },
                );
            },
            qr{zaken/betrokkene/set/not_found},
            'Requesting a non-existent betrokkene results in the correct exception',
        );
    }

    {
        my $override1 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_create_nieuw',
            sub {
                my $self = shift;
                my $betrokkene_info = shift;

                cmp_deeply(
                    $betrokkene_info,
                    {
                        betrokkene_type => 'tester',
                        verificatie     => 'geen',
                        create          => { create => 'me' },
                    },
                    'Correct betrokkene_info passed to _betrokkene_create_nieuw',
                );

                $betrokkene_info->{betrokkene} = 'natuurlijk_persoon-100';

                return 100;
            },
        );
        my $override2 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_get_handle',
            sub {
                return 101;
            },
        );

        my $opts = {};
        my $rv1 = $mock->betrokkene_set(
            $opts,
            'aanvrager',
            {
                betrokkene_type => 'tester',
                verificatie     => 'geen',
                create          => { create => 'me' },
            },
        );

        is($rv1, 101, 'betrokkene_set with "create" options returns correct value');

        # This seems like a pre-existing bug in the "side-effect" changing of
        # $opts by betrokkene_set.
        is($opts->{aanvrager_gm_id}, 100, 'No "aanvrager_gm_id" returned');
        is($opts->{aanvrager}, 101, 'Correct "aanvrager" returned');
    }

    {
        my $override1 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_create_nieuw',
            sub {
                my $self = shift;
                my $betrokkene_info = shift;

                cmp_deeply(
                    $betrokkene_info,
                    {
                        betrokkene_type => 'tester',
                        verificatie     => 'geen',
                        create          => { create => 'me' },
                    },
                    'Correct betrokkene_info passed to _betrokkene_create_nieuw',
                );

                return 100;
            },
        );
        my $override2 = override(
            'Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak::_betrokkene_get_handle',
            sub {
                die ":(";
            },
        );

        my $opts = {};
        throws_ok(
            sub {
                $mock->betrokkene_set(
                    $opts,
                    'aanvrager',
                    {
                        betrokkene_type => 'tester',
                        verificatie     => 'geen',
                        create          => { create => 'me' },
                    },
                );
            },
            qr{zaken/betrokkene/set/not_found},
            'Creating a betrokkene that then does not exist (...) results in the correct exception',
        );
    }
}

package Zaaksysteem::Test::Zaken::Betrokkenen::MockZaak {
    use Moose;
    use Zaaksysteem::Test;

    with 'MooseX::Log::Log4perl', 'Zaaksysteem::Zaken::Betrokkenen';

    sub result_source {
        return (mock_one());
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
