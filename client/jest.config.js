module.exports = {
  // https://github.com/npm/npm/issues/4531
  cacheDirectory: '/tmp',
  coverageDirectory: '<rootDir>/test/coverage',
  // This should correspond with the property keys used in the
  // `DefinePlugin` configuration object in `webpack/config/app/plugins.js`:
  globals: {
    ENV: false,
  },
  moduleNameMapper: {
    '/propCheck$': '<rootDir>/test/__mocks__/propCheck.js',
    // Specific webpack raw loader stub for scss (reads from the file system):
    '/styles/_breakpoints\\.scss$': '<rootDir>/test/__mocks__/breakpoints.js',
    // Generic return values for otherwise ignored
    // imports that would throw a syntax error:
    '\\.(html)$': '<rootDir>/test/__mocks__/htmlMock.js',
    '\\.(css|scss)$': '<rootDir>/test/__mocks__/styleMock.js',
  },
  // setup for fakes/mocks/stubs and such
  setupTestFrameworkScriptFile: '<rootDir>/test/setup.js',
  testMatch: [
    // Old karma tests:
    '<rootDir>/src/**/test.js',
    // New tests:
    '<rootDir>/src/**/*.test.js',
  ],
};
