// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import first from 'lodash/head';
import get from 'lodash/get';
import resourceModule from '../../../api/resource/index.js';
import getRequestOptionsFactory from './getRequestOptionsFactory';
import isUndefined from 'lodash/isUndefined';
import omitBy from 'lodash/omitBy';

const groupingTypes = {
  none: undefined,
  dossier_name: 'Naam',
};
const getQuery = (query, groupingType, queryType) => {
  const escapedQuery = query
    .replace(/\\/g, '\\\\')
    .replace(/\//g, '//')
    .replace(/;/g, '%3B');
  const queries = {
    none: {
      default: `Naam:"${escapedQuery}" OR Omschrijving:"${escapedQuery}"`,
      alternative: `Externe_identificatiekenmerken_nummer_binnen_systeem:"${escapedQuery}"`,
      related: `Externe_identificatiekenmerken_nummer_binnen_systeem:${escapedQuery}`,
    },
    dossier_name: {
      default: `Naam:"${escapedQuery}" OR Omschrijving:"${escapedQuery}"`,
      alternative: `Naam:"${escapedQuery}"`,
      related: `Externe_identificatiekenmerken_nummer_binnen_systeem:${escapedQuery}`,
    },
  };

  return queries[groupingType][queryType];
};

export default angular
  .module('shared.ui.zsSpotEnlighter.externalSearchService', [resourceModule])
  .factory('externalSearchService', [
    '$rootScope',
    '$timeout',
    'resource',
    ($rootScope, $timeout, resource) => {
      const tokenRefresh = 3600 * 1000;

      const apiResource = resource(
        {
          url: '/api/v1/sysin/interface/get_by_module_name/next2know',
        },
        {
          scope: $rootScope,
          cache: {
            every: tokenRefresh,
          },
        }
      ).reduce((requestOptions, data) =>
        get(first(data), 'instance.interface_config')
      );

      apiResource.subscribe((data) => {
        $timeout(() => {
          apiResource.reload();
        }, get(data, 'token.expires_in') * 1000);
      });

      const getRequestParameters = getRequestOptionsFactory(apiResource.data);

      const isDataResolved = () =>
        apiResource.state() === 'resolved' && apiResource.data() !== undefined;

      // The ZS Backend will return a token for Next2Know even if the
      // currently logged in ZS user is not known in Next2Know.
      // As a result this function will return true even if the currently
      // logged in ZS user can't actually search Next2Know.
      // We deal with this in zsSpotEnlighter/index.js in the error
      // handling of the externalSearchResource.
      // More information: ZS-14874.
      const conditionsMet = () =>
        isDataResolved() && apiResource.data().token !== null;

      function delegate(getValue, ...conditions) {
        if (conditions.every((value) => value)) {
          return getValue();
        }

        return null;
      }

      return {
        /**
         * @returns {Object|null}
         */
        getApiInfo() {
          return delegate(() => apiResource.data(), isDataResolved());
        },

        /**
         * Search by keyword
         * ZS-TODO:
         * - why is the first argument optional, and can we safely assign a default value?
         * @param {string} q search query
         * @param {string} [searchIndex = '_all']
         * @param {string} externalSearchResultGroupingType
         * @returns {*}
         */

        getRequestOptions(q, searchIndex = '_all', queryType) {
          const apiInfo = this.getApiInfo();
          const groupingType = get(apiInfo, 'result_grouping') || 'none';
          const query = getQuery(q, groupingType, queryType);
          const size = queryType === 'alternative' ? 500 : 100;
          const params = omitBy(
            {
              q: query,
              size,
              group_by:
                queryType === 'default'
                  ? groupingTypes[groupingType]
                  : undefined,
            },
            isUndefined
          );

          return delegate(
            () =>
              getRequestParameters(`/documents/${searchIndex}/search`, {
                params,
              }),
            query,
            conditionsMet()
          );
        },

        /**
         * Get single search result
         * @param searchIndex
         * @param documentId
         * @returns {*}
         */
        getDocumentRequestOptions(searchIndex, documentId) {
          return delegate(
            () =>
              getRequestParameters(
                `/documents/${searchIndex}/${documentId}?format=array`
              ),
            searchIndex,
            documentId,
            conditionsMet()
          );
        },

        /**
         * Download a file from next2know
         * @param filename
         * @returns {*}
         */
        getDownloadRequestOptions(filename) {
          return delegate(
            () =>
              getRequestParameters(`/files/${filename}`, {
                headers: {
                  accept: 'application/octet-stream',
                },
                responseType: 'arraybuffer',
              }),
            filename,
            conditionsMet()
          );
        },

        /**
         * Get a thumbnail from next2know
         * @param searchIndex
         * @param documentId
         * @returns {*}
         */
        getDocumentThumbRequestOptions(searchIndex, documentId) {
          return delegate(
            () =>
              getRequestParameters(`/thumbnails/${searchIndex}/${documentId}`),
            searchIndex,
            documentId,
            conditionsMet()
          );
        },
      };
    },
  ]).name;
