// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
export default (instance) => {
  return {
    validations: mapValues(
      pickBy(instance, (value) => value.validity !== 'valid'),
      (value) => {
        return [
          {
            [value.validity]: value.message,
          },
        ];
      }
    ),
    valid: false,
  };
};
