// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (current) => {
  let parents = [],
    parent = current;

  while (parent) {
    parents.push(parent);
    parent = parent.parent;
  }

  return parents;
};
