// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import oneWayBind from '../../../shared/util/oneWayBind';

export default angular
  .module('Zaaksysteem.meeting.proposalListService', [])
  .factory('proposalListService', () => {
    return {
      areItemsGrouped: oneWayBind(),
    };
  }).name;
