// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default (resource, scope, valueGetter) => {
  return resource(
    () => {
      let values = valueGetter();

      return values.role
        ? {
            url: values.caseId
              ? `/zaak/${values.caseId}/update/betrokkene/suggestion`
              : '/form/register_relaties/suggestion',
            params: {
              rol: values.role,
            },
          }
        : null;
    },
    { scope, cache: { disabled: true } }
  );
};
