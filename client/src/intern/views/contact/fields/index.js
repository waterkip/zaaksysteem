// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import company from './company';
import person from './person';
import employee from './employee';

export default {
  company,
  person,
  employee,
};
