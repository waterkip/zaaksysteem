BEGIN;

ALTER TABLE natuurlijk_persoon ADD COLUMN naamgebruik TEXT;
ALTER TABLE gm_natuurlijk_persoon ADD COLUMN naamgebruik TEXT;

COMMIT;
