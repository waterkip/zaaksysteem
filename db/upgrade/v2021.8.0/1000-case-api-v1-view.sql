BEGIN;

  DROP MATERIALIZED VIEW IF EXISTS casetype_v1_reference CASCADE;

  CREATE MATERIALIZED VIEW casetype_v1_reference AS
  SELECT
    zt.id as id,
    ztn.titel as title,
    zt.uuid as uuid,
    ztn.version as version,
    ztn.id as casetype_node_id
  FROM
    zaaktype zt
  JOIN
    zaaktype_node ztn
  ON ztn.zaaktype_id = zt.id
  ;

  DROP TRIGGER IF EXISTS refresh_casetype_v1_reference ON "zaaktype";

  CREATE OR REPLACE FUNCTION refresh_casetype_v1_reference ()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
  BEGIN
    REFRESH MATERIALIZED VIEW casetype_v1_reference;
    RETURN NULL;
  END
  $$;

  CREATE TRIGGER refresh_casetype_v1_reference
  AFTER INSERT
  OR UPDATE
  OR DELETE
  OR TRUNCATE ON "zaaktype" FOR EACH statement
  EXECUTE PROCEDURE refresh_casetype_v1_reference ();

  DROP VIEW IF EXISTS case_v1;

  CREATE VIEW case_v1 AS
  SELECT
    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.stalled_until AS stalled_until,
    z.archival_state AS archival_state,
    zm.stalled_since AS stalled_since,
    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    jsonb_object_agg(ca.magic_string, ca.value) AS attributes,

    -- if result unset null
    -- else zaaktype result join
    null AS result,
    null AS result_id,
    null AS active_selection_list,
    -- if result
    -- json blob
    null AS case_outcome,

    json_build_object(
      'preview', ct_ref.title,
      'reference', ct_ref.uuid,
      'instance', json_build_object(
        'version', ct_ref.version,
        'name', ct_ref.title
      ),
      'type', 'casetype'
    ) AS casetype,

    -- routing info
    null AS group,
    null AS role,

    -- Look into logging, but it is also in zaak meta
    -- Python, what are you using?
    null AS suspension_rationale,
    -- investigate..
    null AS premature_completion_rationale,

    -- Subject API/v1 shit, mind boggling
    null AS requestor,
    null AS assignee,
    null AS coordinator,

    -- phase can volgende fase, show the next phase number
    null AS phase,

    -- milestones, similar to phase
    -- json blob
    null AS milestone,

    -- complex queries here
    null AS relations,
    null AS case_relationships,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z
  JOIN zaak_meta zm
  ON zm.zaak_id = z.id
  JOIN case_attributes ca
  ON  ca.case_id = z.id
  JOIN casetype_v1_reference ct_ref
  ON z.zaaktype_node_id = ct_ref.casetype_node_id
  GROUP BY z.id, zm.stalled_since, zm.current_deadline, zm.deadline_timeline, ct_ref.title, ct_ref.uuid, ct_ref.version
  ;

COMMIT;
