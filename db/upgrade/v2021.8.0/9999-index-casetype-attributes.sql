BEGIN;

  DROP INDEX IF EXISTS zaaktype_kenmerken_zaaktype_node_idx;
  CREATE INDEX zaaktype_kenmerken_zaaktype_node_idx on zaaktype_kenmerken(zaaktype_node_id);

  DROP INDEX IF EXISTS file_case_documents_case_idx;
  CREATE INDEX file_case_documents_case_idx on file_case_document(case_id);

  DROP INDEX IF EXISTS file_case_documents_case_library_idx;
  CREATE INDEX file_case_documents_case_library_idx on file_case_document(case_id);

  DROP INDEX IF EXISTS casetype_v1_reference_casetype_node_idx;
  CREATE INDEX casetype_v1_reference_casetype_node_idx ON casetype_v1_reference(casetype_node_id);

COMMIT;
