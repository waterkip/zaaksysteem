BEGIN;

  CREATE OR REPLACE FUNCTION migrate_contactmoments() RETURNS void LANGUAGE plpgsql AS $$
  DECLARE
    r record;
    h hstore;
    thread_id int;
    thread_contactmoment_id int;
    thread_msg_id int;
  BEGIN
        FOR r IN
          SELECT
            get_subject_by_legacy_id(cm.subject_id) subject_uuid,
            CASE
              WHEN cm.medium = 'balie' THEN 'frontdesk'
              WHEN cm.medium = 'behandelaar' THEN 'assignee'
              WHEN cm.medium = 'email' THEN 'email'
              WHEN cm.medium = 'post' THEN 'mail'
              WHEN cm.medium = 'telefoon' THEN 'phone'
              WHEN cm.medium = 'webformulier' THEN 'webform'

              /* This is not possible due to failing DB constraints on
               * this table, see
               * lib/Zaaksysteem/Schema/Contactmoment.pm contactmoment_medium
               * for more information
              WHEN cm.medium = 'sociale media' THEN 'social_media'
              */

            END medium,
            cm.date_created date_created,
            get_subject_by_legacy_id(cm.created_by) created_by,
            note.message message,
            substr(note.message, 0, 180) message_slug,
            cm.case_id case_id,
            cm.id id
          FROM
            contactmoment_note note
          JOIN
            contactmoment cm
          ON
            cm.id = note.contactmoment_id
          WHERE
            cm.type = 'note'
          ORDER BY cm.id asc
        LOOP
          h := hstore(r);

          RAISE NOTICE 'Migrating contact moment with id %', r.id;

          SELECT INTO thread_id migrate_contactmoment_to_thread(h);
          RAISE NOTICE 'Created thread with id %', thread_id;

          SELECT INTO thread_contactmoment_id migrate_contactmoment_to_thread_message_type(h);
          RAISE NOTICE 'Created contact moment with id %', thread_contactmoment_id;

          SELECT INTO thread_msg_id migrate_contactmoment_to_thread_message(thread_id, thread_contactmoment_id, h);
          RAISE NOTICE 'Created thread message with id %', thread_msg_id;

        END LOOP;
        RETURN;

  END $$;

  CREATE OR REPLACE FUNCTION migrate_contactmoment_to_thread(
    IN contactmoment hstore,
    OUT thread_id int
  ) LANGUAGE plpgsql AS $$
  DECLARE
    case_id int;
    dn TEXT;
    msg_jsonb jsonb;
    s_uuid uuid;
    created_by TEXT;
    mytime TIMESTAMP;
  BEGIN

    SELECT INTO created_by get_subject_display_name_by_uuid(uuid(contactmoment->'created_by'));

    s_uuid := uuid(contactmoment->'subject_uuid');
    SELECT INTO dn get_subject_display_name_by_uuid(s_uuid);


    mytime := CAST(contactmoment->'date_created' AS TIMESTAMP);

    IF contactmoment->'case_id' IS NOT NULL
    THEN
      case_id := contactmoment->'case_id';
    END IF;

    msg_jsonb := json_build_object(
      'message_type', 'contact_moment',
      'slug', contactmoment->'message_slug',
      'created_name', created_by,
      'created', to_char(mytime, 'IYYY-MM-DD"T"HH24:MI:SS+00:00'),
      'direction', null,
      'channel', contactmoment->'medium',
      'recipient_name', dn
    );

    -- Display name is mandatory not null, but we don't always have it
    IF dn IS NULL
    THEN
      dn := '';
    END IF;

    INSERT INTO thread(
      contact_uuid,
      contact_displayname,
      case_id,
      created,
      last_modified,
      thread_type,
      last_message_cache
    )
    VALUES (
      s_uuid,
      dn,
      case_id,
      mytime,
      mytime,
      'contact_moment',
      msg_jsonb::text
    )
    RETURNING id INTO thread_id;

    RETURN;
  END $$;

  CREATE OR REPLACE FUNCTION migrate_contactmoment_to_thread_message_type(
    IN contactmoment hstore,
    OUT contactmoment_id int
  ) LANGUAGE plpgsql as $$
  DECLARE
    dn TEXT;
    s_uuid uuid;
    case_id int;
    msg TEXT;
  BEGIN

    -- The create will fail and that's it
    IF contactmoment->'medium' IS NULL
    THEN
      RAISE NOTICE 'Empty medium in contactmoment %', contactmoment->'id';
    END IF;

    IF contactmoment->'subject_uuid' IS NULL
    THEN
	s_uuid := uuid(contactmoment->'created_by');
    ELSE
	s_uuid := uuid(contactmoment->'subject_uuid');
    END IF;
    SELECT INTO dn get_subject_display_name_by_uuid(s_uuid);

    INSERT INTO thread_message_contact_moment(
      content,
      contact_channel,
      recipient_uuid,
      recipient_displayname
    )
    VALUES (
      contactmoment->'message',
      contactmoment->'medium',
      s_uuid,
      dn
    )
    RETURNING id INTO contactmoment_id;

  END $$;

  CREATE OR REPLACE FUNCTION migrate_contactmoment_to_thread_message(
    IN thread_id int,
    IN thread_msg_id int,
    IN contactmoment hstore,
    OUT created_id int
  ) LANGUAGE plpgsql AS $$
  DECLARE
    dn TEXT;
    s_uuid uuid;
    case_id int;
  BEGIN

    s_uuid := uuid(contactmoment->'created_by');
    SELECT INTO dn get_subject_display_name_by_uuid(s_uuid);

    INSERT INTO thread_message (
      thread_id,
      type,
      message_slug,
      created_by_uuid,
      created_by_displayname,
      created,
      last_modified,
      thread_message_contact_moment_id
    )
    VALUES (
      thread_id,
      'contact_moment',
      contactmoment->'message_slug',
      s_uuid,
      dn,
      CAST(contactmoment->'date_created' AS TIMESTAMP),
      CAST(contactmoment->'date_created' AS TIMESTAMP),
      thread_msg_id
    )
    RETURNING id INTO created_id;

    RETURN;
  END $$;

  ALTER TABLE thread DISABLE trigger thread_insert_trigger;
  ALTER TABLE thread_message DISABLE trigger thread_message_insert_trigger;

  SELECT migrate_contactmoments();

  ALTER TABLE thread ENABLE trigger thread_insert_trigger;
  ALTER TABLE thread_message ENABLE trigger thread_message_insert_trigger;


  DROP FUNCTION migrate_contactmoments();
  DROP FUNCTION migrate_contactmoment_to_thread(hstore);
  DROP FUNCTION migrate_contactmoment_to_thread_message_type(hstore);
  DROP FUNCTION migrate_contactmoment_to_thread_message(int, int, hstore);

COMMIT;
