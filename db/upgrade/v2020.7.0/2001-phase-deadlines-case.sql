BEGIN;

  ALTER TABLE zaak ADD COLUMN current_deadline  JSONB default '{}'::jsonb NOT NULL;
  ALTER TABLE zaak ADD COLUMN deadline_timeline JSONB default '[]'::jsonb NOT NULL;

COMMIT;
