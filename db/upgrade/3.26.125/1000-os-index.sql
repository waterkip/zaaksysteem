    DROP INDEX IF EXISTS np_bsn_idx;
    CREATE INDEX CONCURRENTLY np_bsn_idx
        ON natuurlijk_persoon ((NULLIF(burgerservicenummer::text, '')::int));

    DROP INDEX IF EXISTS np_bsn_active_idx;
    CREATE INDEX CONCURRENTLY np_bsn_active_idx
        ON natuurlijk_persoon
            ((NULLIF(burgerservicenummer::text, '')::int), active)
        WHERE deleted_on is null;

    DROP INDEX IF EXISTS np_bsn_active_no_text_idx;
    CREATE INDEX CONCURRENTLY np_bsn_active_no_text_idx
        ON natuurlijk_persoon ((NULLIF(burgerservicenummer, '')::int), active)
        WHERE deleted_on is null;


    DROP INDEX IF EXISTS case_action_case_id_idx;
    CREATE INDEX CONCURRENTLY case_action_case_id_idx ON case_action (case_id);

    DROP INDEX IF EXISTS case_action_casetype_status_id_idx;
    CREATE INDEX CONCURRENTLY case_action_casetype_status_id_idx
        ON case_action (casetype_status_id);

    DROP INDEX IF EXISTS case_action_case_id_and_casetype_status_id_idx;
    CREATE INDEX CONCURRENTLY case_action_case_id_and_casetype_status_id_idx
        ON case_action (case_id, casetype_status_id);

    DROP INDEX IF EXISTS adres_natuurlijk_persoon_id_idx;
    CREATE INDEX CONCURRENTLY adres_natuurlijk_persoon_id_idx
        ON adres (natuurlijk_persoon_id);

    DROP INDEX IF EXISTS gm_adres_natuurlijk_persoon_id_idx;
    CREATE INDEX CONCURRENTLY gm_adres_natuurlijk_persoon_id_idx
        ON gm_adres (natuurlijk_persoon_id);


    DROP INDEX IF EXISTS object_subscripton_local_id_idx;
    CREATE INDEX CONCURRENTLY object_subscripton_local_id_idx
        ON object_subscription ((local_id::NUMERIC));

    DROP INDEX IF EXISTS object_subscripton_local_table_idx;
    CREATE INDEX CONCURRENTLY object_subscripton_local_table_idx
        ON object_subscription (local_table);

    DROP INDEX IF EXISTS object_subscripton_local_table_and_id_idx;
    CREATE INDEX CONCURRENTLY object_subscripton_local_table_and_id_idx
        ON object_subscription ((local_id::NUMERIC), local_table);

    DROP INDEX IF EXISTS object_acl_entity_type_idx;
    CREATE INDEX CONCURRENTLY object_acl_entity_type_idx ON object_acl_entry(entity_type);

    DROP INDEX IF EXISTS object_acl_entity_type_id_and_scope_idx;
    CREATE INDEX CONCURRENTLY object_acl_entity_type_id_and_scope_idx
        ON object_acl_entry( entity_id, entity_type, scope);

    DROP INDEX IF EXISTS object_acl_entity_type_id_where_scope_idx;
    CREATE INDEX CONCURRENTLY object_acl_entity_type_id_where_scope_idx
        ON object_acl_entry( entity_id, entity_type) where scope = 'instance';

    DROP INDEX IF EXISTS zaaktype_kenmerken_group_and_node_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_kenmerken_group_and_node_id_idx
        ON zaaktype_kenmerken
            (is_group, zaaktype_node_id, bibliotheek_kenmerken_id)
        WHERE is_group = false;

    DROP INDEX IF EXISTS file_filestore_id_idx;
    CREATE INDEX CONCURRENTLY file_filestore_id_idx ON file (id, filestore_id);

    DROP INDEX IF EXISTS file_filestore_id_no_case_idx;
    CREATE INDEX CONCURRENTLY file_filestore_id_no_case_idx
        ON file (id, filestore_id) where case_id is null;

    DROP INDEX IF EXISTS filestore_virus_found_idx;
    CREATE INDEX CONCURRENTLY filestore_virus_found_idx
        ON filestore (uuid) WHERE virus_scan_status !~~ 'found%'::text;

    DROP INDEX IF EXISTS zaaktype_kenmerken_node_no_permissions;
    CREATE INDEX CONCURRENTLY zaaktype_kenmerken_node_no_permissions
        ON zaaktype_kenmerken (zaaktype_node_id)
        WHERE required_permissions is NULL or required_permissions = '{}';

    DROP INDEX IF EXISTS zaaktype_kenmerken_zaakstatus_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_kenmerken_zaakstatus_id_idx
        ON zaaktype_kenmerken (zaak_status_id);

    DROP INDEX IF EXISTS zaaktype_kenmerken_zaaktype_node_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_kenmerken_zaaktype_node_id_idx
        ON zaaktype_kenmerken (zaaktype_node_id);

    DROP INDEX IF EXISTS zaaktype_regel_zaaktype_node_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_regel_zaaktype_node_id_idx
        ON zaaktype_regel (zaaktype_node_id, active, is_group)
        WHERE active = true and is_group = false;

    DROP INDEX IF EXISTS zaaktype_regel_name_zaaktype_node_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_regel_name_zaaktype_node_id_idx
        ON zaaktype_regel (zaaktype_node_id, active, is_group)
        WHERE active = true and is_group = false and naam is not null;

    DROP INDEX IF EXISTS zaaktype_status_status_id_idx;
    CREATE INDEX CONCURRENTLY zaaktype_status_status_id_idx ON zaaktype_status (status);

    DROP INDEX IF EXISTS bibliotheek_kenmerken_magic_string_value_type_idx;
    CREATE INDEX CONCURRENTLY bibliotheek_kenmerken_magic_string_value_type_idx
        ON bibliotheek_kenmerken (magic_string, value_type);

    DROP INDEX IF EXISTS file_lock_subject_name_idx;
    CREATE INDEX CONCURRENTLY file_lock_subject_name_idx
        ON file (accepted, case_id)
        WHERE date_deleted is null and destroyed is false;

BEGIN;

    ALTER TABLE queue DROP CONSTRAINT queue_parent_id_fkey;
    ALTER TABLE queue ADD CONSTRAINT queue_parent_id_fkey
        FOREIGN KEY (parent_id) REFERENCES public.queue(id) ON DELETE SET NULL;

COMMIT;

