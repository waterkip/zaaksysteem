BEGIN;

    CREATE OR REPLACE FUNCTION insert_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            NEW.created = NOW();
            NEW.last_modified = NOW();
            RETURN NEW;
        END;
    $$;

    CREATE OR REPLACE FUNCTION update_timestamps() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
        BEGIN
            NEW.last_modified = NOW();
            RETURN NEW;
        END;
    $$;

    DROP TRIGGER IF EXISTS bibliotheek_notificaties_insert_trigger ON bibliotheek_notificaties;
    CREATE TRIGGER bibliotheek_notificaties_insert_trigger
        BEFORE INSERT ON bibliotheek_notificaties
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_notificaties_update_trigger ON bibliotheek_notificaties;
    CREATE TRIGGER bibliotheek_notificaties_update_trigger
        BEFORE UPDATE ON bibliotheek_notificaties
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_categorie_insert_trigger ON bibliotheek_categorie;
    CREATE TRIGGER bibliotheek_categorie_insert_trigger
        BEFORE INSERT ON bibliotheek_categorie
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_categorie_update_trigger ON bibliotheek_categorie;
    CREATE TRIGGER bibliotheek_categorie_update_trigger
        BEFORE UPDATE ON bibliotheek_categorie
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_kenmerken_insert_trigger ON bibliotheek_kenmerken;
    CREATE TRIGGER bibliotheek_kenmerken_insert_trigger
        BEFORE INSERT ON bibliotheek_kenmerken
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_kenmerken_update_trigger ON bibliotheek_kenmerken;
    CREATE TRIGGER bibliotheek_kenmerken_update_trigger
        BEFORE UPDATE ON bibliotheek_kenmerken
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_sjablonen_insert_trigger ON bibliotheek_sjablonen;
    CREATE TRIGGER bibliotheek_sjablonen_insert_trigger
        BEFORE INSERT ON bibliotheek_sjablonen
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS bibliotheek_sjablonen_update_trigger ON bibliotheek_sjablonen;
    CREATE TRIGGER bibliotheek_sjablonen_update_trigger
        BEFORE UPDATE ON bibliotheek_sjablonen
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS bedrijf_authenticatie_insert_trigger ON bedrijf_authenticatie;
    CREATE TRIGGER bedrijf_authenticatie_insert_trigger
        BEFORE INSERT ON bedrijf_authenticatie
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS bedrijf_authenticatie_update_trigger ON bedrijf_authenticatie;
    CREATE TRIGGER bedrijf_authenticatie_update_trigger
        BEFORE UPDATE ON bedrijf_authenticatie
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS beheer_import_insert_trigger ON beheer_import;
    CREATE TRIGGER beheer_import_insert_trigger
        BEFORE INSERT ON beheer_import
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS beheer_import_update_trigger ON beheer_import;
    CREATE TRIGGER beheer_import_update_trigger
        BEFORE UPDATE ON beheer_import
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS beheer_import_log_insert_trigger ON beheer_import_log;
    CREATE TRIGGER beheer_import_log_insert_trigger
        BEFORE INSERT ON beheer_import_log
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS beheer_import_log_update_trigger ON beheer_import_log;
    CREATE TRIGGER beheer_import_log_update_trigger
        BEFORE UPDATE ON beheer_import_log
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS betrokkene_notes_insert_trigger ON betrokkene_notes;
    CREATE TRIGGER betrokkene_notes_insert_trigger
        BEFORE INSERT ON betrokkene_notes
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS betrokkene_notes_update_trigger ON betrokkene_notes;
    CREATE TRIGGER betrokkene_notes_update_trigger
        BEFORE UPDATE ON betrokkene_notes
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS contact_data_insert_trigger ON contact_data;
    CREATE TRIGGER contact_data_insert_trigger
        BEFORE INSERT ON contact_data
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS contact_data_update_trigger ON contact_data;
    CREATE TRIGGER contact_data_update_trigger
        BEFORE UPDATE ON contact_data
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS logging_insert_trigger ON logging;
    CREATE TRIGGER logging_insert_trigger
        BEFORE INSERT ON logging
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS logging_update_trigger ON logging;
    CREATE TRIGGER logging_update_trigger
        BEFORE UPDATE ON logging
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS parkeergebied_insert_trigger ON parkeergebied;
    CREATE TRIGGER parkeergebied_insert_trigger
        BEFORE INSERT ON parkeergebied
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS parkeergebied_update_trigger ON parkeergebied;
    CREATE TRIGGER parkeergebied_update_trigger
        BEFORE UPDATE ON parkeergebied
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS scheduled_jobs_insert_trigger ON scheduled_jobs;
    CREATE TRIGGER scheduled_jobs_insert_trigger
        BEFORE INSERT ON scheduled_jobs
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS scheduled_jobs_update_trigger ON scheduled_jobs;
    CREATE TRIGGER scheduled_jobs_update_trigger
        BEFORE UPDATE ON scheduled_jobs
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_insert_trigger ON zaaktype;
    CREATE TRIGGER zaaktype_insert_trigger
        BEFORE INSERT ON zaaktype
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_update_trigger ON zaaktype;
    CREATE TRIGGER zaaktype_update_trigger
        BEFORE UPDATE ON zaaktype
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_authorisation_insert_trigger ON zaaktype_authorisation;
    CREATE TRIGGER zaaktype_authorisation_insert_trigger
        BEFORE INSERT ON zaaktype_authorisation
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_authorisation_update_trigger ON zaaktype_authorisation;
    CREATE TRIGGER zaaktype_authorisation_update_trigger
        BEFORE UPDATE ON zaaktype_authorisation
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_betrokkenen_insert_trigger ON zaaktype_betrokkenen;
    CREATE TRIGGER zaaktype_betrokkenen_insert_trigger
        BEFORE INSERT ON zaaktype_betrokkenen
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_betrokkenen_update_trigger ON zaaktype_betrokkenen;
    CREATE TRIGGER zaaktype_betrokkenen_update_trigger
        BEFORE UPDATE ON zaaktype_betrokkenen
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_kenmerken_insert_trigger ON zaaktype_kenmerken;
    CREATE TRIGGER zaaktype_kenmerken_insert_trigger
        BEFORE INSERT ON zaaktype_kenmerken
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_kenmerken_update_trigger ON zaaktype_kenmerken;
    CREATE TRIGGER zaaktype_kenmerken_update_trigger
        BEFORE UPDATE ON zaaktype_kenmerken
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_node_insert_trigger ON zaaktype_node;
    CREATE TRIGGER zaaktype_node_insert_trigger
        BEFORE INSERT ON zaaktype_node
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_node_update_trigger ON zaaktype_node;
    CREATE TRIGGER zaaktype_node_update_trigger
        BEFORE UPDATE ON zaaktype_node
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_notificatie_insert_trigger ON zaaktype_notificatie;
    CREATE TRIGGER zaaktype_notificatie_insert_trigger
        BEFORE INSERT ON zaaktype_notificatie
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_notificatie_update_trigger ON zaaktype_notificatie;
    CREATE TRIGGER zaaktype_notificatie_update_trigger
        BEFORE UPDATE ON zaaktype_notificatie
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_regel_insert_trigger ON zaaktype_regel;
    CREATE TRIGGER zaaktype_regel_insert_trigger
        BEFORE INSERT ON zaaktype_regel
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_regel_update_trigger ON zaaktype_regel;
    CREATE TRIGGER zaaktype_regel_update_trigger
        BEFORE UPDATE ON zaaktype_regel
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_relatie_insert_trigger ON zaaktype_relatie;
    CREATE TRIGGER zaaktype_relatie_insert_trigger
        BEFORE INSERT ON zaaktype_relatie
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_relatie_update_trigger ON zaaktype_relatie;
    CREATE TRIGGER zaaktype_relatie_update_trigger
        BEFORE UPDATE ON zaaktype_relatie
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_resultaten_insert_trigger ON zaaktype_resultaten;
    CREATE TRIGGER zaaktype_resultaten_insert_trigger
        BEFORE INSERT ON zaaktype_resultaten
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_resultaten_update_trigger ON zaaktype_resultaten;
    CREATE TRIGGER zaaktype_resultaten_update_trigger
        BEFORE UPDATE ON zaaktype_resultaten
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_sjablonen_insert_trigger ON zaaktype_sjablonen;
    CREATE TRIGGER zaaktype_sjablonen_insert_trigger
        BEFORE INSERT ON zaaktype_sjablonen
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_sjablonen_update_trigger ON zaaktype_sjablonen;
    CREATE TRIGGER zaaktype_sjablonen_update_trigger
        BEFORE UPDATE ON zaaktype_sjablonen
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_status_insert_trigger ON zaaktype_status;
    CREATE TRIGGER zaaktype_status_insert_trigger
        BEFORE INSERT ON zaaktype_status
        FOR EACH ROW
        EXECUTE PROCEDURE insert_timestamps();

    DROP TRIGGER IF EXISTS zaaktype_status_update_trigger ON zaaktype_status;
    CREATE TRIGGER zaaktype_status_update_trigger
        BEFORE UPDATE ON zaaktype_status
        FOR EACH ROW
        EXECUTE PROCEDURE update_timestamps();

COMMIT;
