
BEGIN;
  CREATE INDEX queue_type_idx ON queue USING btree(type);
  CREATE INDEX queue_dupe_token_case_form_idx ON queue((data::jsonb->'create_args'->>'duplicate_prevention_token')) where type ='create_case_form';
COMMIT;

