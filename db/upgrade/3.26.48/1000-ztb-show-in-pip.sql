BEGIN;

    ALTER TABLE zaaktype_relatie ADD COLUMN show_in_pip BOOLEAN DEFAULT false NOT NULL;
    ALTER TABLE zaaktype_relatie ADD COLUMN pip_label TEXT;

COMMIT;
