BEGIN;

-- Creating new tables for geo feature/service
CREATE TABLE service_geojson (
    id int GENERATED ALWAYS AS IDENTITY,
    uuid UUID NOT NULL,
    geo_json JSONB NOT NULL,
    PRIMARY KEY( id ),
    UNIQUE( uuid )
);

CREATE TABLE service_geojson_relationship (
	id int GENERATED ALWAYS AS IDENTITY,
	service_geojson_id INT NOT NULL,
    related_uuid UUID NOT NULL,

	PRIMARY KEY( id ),
    FOREIGN KEY(service_geojson_id)
        REFERENCES service_geojson(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,

    UNIQUE(related_uuid, service_geojson_id)
);

COMMIT;
