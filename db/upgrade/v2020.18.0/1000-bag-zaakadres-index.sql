BEGIN;

  DROP INDEX IF EXISTS zaaktype_kenmerken_bag_zaakadres_or_map_idx;

  create index zaaktype_kenmerken_bag_zaakadres_or_map_idx on zaaktype_kenmerken(bag_zaakadres) where bag_zaakadres = '1' or properties::jsonb @> '{"map_case_location": "1"}'::jsonb;

  DROP INDEX IF EXISTS zaak_bag_type_id_idx;

  CREATE INDEX zaak_bag_type_id_idx on zaak_bag(bag_type, bag_id);

COMMIT;
