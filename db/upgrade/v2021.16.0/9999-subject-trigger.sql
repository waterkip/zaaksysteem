BEGIN;

DROP TRIGGER IF EXISTS refresh_subject_position_matrix ON subject;

CREATE TRIGGER refresh_subject_position_matrix
  AFTER INSERT
  OR UPDATE OF role_ids, group_ids, subject_type, uuid, username
  OR DELETE
  OR TRUNCATE ON "subject" FOR EACH statement
  EXECUTE PROCEDURE refresh_subject_position_matrix ();

COMMIT;
