BEGIN;

  ALTER TABLE zaaktype_node ADD COLUMN v0_json JSONB NOT NULL DEFAULT '{}'::jsonb;

  CREATE OR REPLACE FUNCTION zaaktype_node_as_v0(
    IN ztn hstore,
    OUT unit_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties JSONB;
  BEGIN

    properties := (ztn->'properties')::jsonb;

    SELECT INTO unit_json json_build_object(
      -- properties mapping
      'case.casetype.adjourn_period', properties->'verdagingstermijn',
      'case.casetype.archive_classification_code', properties->'archiefclassicatiecode',
      'case.casetype.designation_of_confidentiality', properties->'vertrouwelijkheidsaanduiding',
      'case.casetype.eform', properties->'e_formulier',
      'case.casetype.extension', properties->'verlenging_mogelijk',
      'case.casetype.extension_period', properties->'verlengingstermijn',
      'case.casetype.goal', properties->'doel',
      'case.casetype.lex_silencio_positivo', properties->'lex_silencio_positivo',
      'case.casetype.motivation', properties->'aanleiding',
      'case.casetype.objection_and_appeal', properties->'beroep_mogelijk',
      'case.casetype.penalty', properties->'wet_dwangsom',
      'case.casetype.price.counter', properties->'pdc_tarief_balie',
      'case.casetype.price.email', properties->'pdc_tarief_email',
      'case.casetype.price.employee', properties->'pdc_tarief_behandelaar',
      'case.casetype.price.post', properties->'pdc_tarief_post',
      'case.casetype.price.telephone', properties->'pdc_tarief_telefoon',
      'case.casetype.principle_local', properties->'lokale_grondslag',
      'case.casetype.publication', properties->'publicatie',
      'case.casetype.registration_bag', properties->'bag',
      'case.casetype.supervisor', properties->'verantwoordelijke',
      'case.casetype.supervisor_relation', properties->'verantwoordingsrelatie',
      'case.casetype.suspension', properties->'opschorten_mogelijk',
      'case.casetype.text_for_publication', properties->'publicatietekst',
      'case.casetype.wkpb', properties->'wkpb',
      -- ztn itself
      'case.casetype.version', (ztn->'version')::int,
      'case.casetype.description', ztn->'zaaktype_omschrijving',
      'case.casetype.name', ztn->'titel',
      'case.casetype.node.id', (ztn->'id')::int,
      'case.casetype.id', (ztn->'zaaktype_id')::int,
      'case.casetype.keywords', ztn->'zaaktype_trefwoorden',
      'case.casetype.identification', ztn->'code',
      'case.casetype.initiator_source', ztn->'trigger',
      'case.casetype.version_date_of_creation', (ztn->'created')::timestamp with time zone,
      -- how is this even possible?
      'case.casetype.version_date_of_expiration', (ztn->'deleted')::timestamp with time zone
    );

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION zaaktype_node_v0_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    BEGIN
      SELECT INTO NEW.v0_json zaaktype_node_as_v0(hstore(NEW));
      RETURN NEW;
    END;
  $$;

  CREATE TRIGGER update_v0_json
  BEFORE INSERT
  OR UPDATE
  ON zaaktype_node
  FOR EACH ROW
  EXECUTE PROCEDURE zaaktype_node_v0_json();

  UPDATE zaaktype_node set id = id;

COMMIT;
