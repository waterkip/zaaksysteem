BEGIN;

    INSERT INTO queue (
        type,
        label,
        priority,
        metadata,
        data
    )
    SELECT
        'touch_case',
        'New case model migration',
        3000,
        '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
        '{"case_object_id":"' || uuid || '"}'
    FROM
        object_data
    WHERE
        object_class = 'case'
        and
        object_id NOT IN (
            select distinct(case_id) from case_property
        );

COMMIT;

