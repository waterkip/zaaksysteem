BEGIN;

  DROP FUNCTION IF EXISTS get_confidential_mapping(varchar);

  CREATE OR REPLACE FUNCTION get_confidential_mapping(
    type confidentiality
  )
  RETURNS JSONB
  STABLE
  LANGUAGE plpgsql
  AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type = 'public'
    THEN
      mapped := 'Openbaar';
    ELSIF type = 'internal'
    THEN
      mapped := 'Intern';
    ELSIF type = 'confidential'
    THEN
      mapped := 'Vertrouwelijk';
    ELSE
      RAISE EXCEPTION 'Unknown confidentiality type %', type;
    END IF;


    return jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;

  CREATE OR REPLACE FUNCTION get_confidential_mapping(
    type text
  )
  RETURNS JSONB
  STABLE
  LANGUAGE plpgsql
  AS $$
  DECLARE
    mapped text;
  BEGIN

    RETURN get_confidential_mapping(type::confidentiality);

  END;
  $$;

COMMIT;

