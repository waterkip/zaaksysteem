BEGIN;

  CREATE OR REPLACE FUNCTION is_destructable(
    destruction_date timestamp with time zone
  )
  RETURNS boolean
  STABLE
  LANGUAGE plpgsql
  AS $$
  BEGIN

    IF destruction_date IS NOT NULL AND destruction_date < NOW()
    THEN
      RETURN true;
    ELSE
      RETURN false;
    END IF;
  END;
  $$;




COMMIT;

