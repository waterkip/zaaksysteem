BEGIN;
CREATE TABLE contact_data_backup_delete_after_2022_6 AS TABLE contact_data;
DELETE FROM contact_data
WHERE id IN (
        SELECT
            id
        FROM (
            SELECT
                id,
                ROW_NUMBER() OVER (PARTITION BY betrokkene_type, gegevens_magazijn_id ORDER BY last_modified DESC, id DESC) AS row_num
            FROM
                contact_data) t
        WHERE
            t.row_num > 1);
CREATE UNIQUE INDEX contact_data_betrokkene_type_gegevens_magazijn_id_idx ON contact_data (betrokkene_type, gegevens_magazijn_id);
COMMIT;

