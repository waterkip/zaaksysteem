BEGIN;
drop table if exists file_annotation;

create table file_annotation (
    id uuid PRIMARY KEY,
    file_id integer not null,
    subject varchar(255) not null,
    properties text not null default '{}',
    created timestamp not null default NOW(),
    modified timestamp
);

ALTER TABLE ONLY file_annotation
    ADD CONSTRAINT file_annotation_file_id_fkey FOREIGN KEY (file_id) REFERENCES file(id);


COMMIT;