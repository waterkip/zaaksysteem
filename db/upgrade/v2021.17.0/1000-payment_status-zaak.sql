BEGIN;

  CREATE OR REPLACE FUNCTION get_payment_status_mapping(
    IN type text,
    OUT mapping JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    mapped text;
  BEGIN

    IF type IS NULL
    THEN
      RETURN;
    ELSIF type = 'pending'
    THEN
      mapped := 'Wachten op bevestiging';
    ELSIF type = 'offline'
    THEN
      mapped := 'Later betalen';
    ELSIF type = 'failed'
    THEN
      mapped := 'Niet geslaagd';
    ELSIF type = 'success'
    THEN
      mapped := 'Geslaagd';
    ELSE
      RAISE EXCEPTION 'Unknown payment status type %', type;
    END IF;

    mapping := jsonb_build_object(
      'original', type,
      'mapped', mapped
    );

  END;
  $$;

COMMIT;

