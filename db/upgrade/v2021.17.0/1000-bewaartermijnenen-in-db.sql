BEGIN;

  CREATE TABLE result_preservation_terms (
    id SERIAL,
    code int NOT NULL UNIQUE,
    label text NOT NULL UNIQUE,
    unit text NOT NULL,
    unit_amount numeric NOT NULL
  );

  INSERT INTO result_preservation_terms (
    code, label, unit_amount, unit
  )
  VALUES
     (28, '4 weken', 4, 'week'),
     (42, '6 weken', 6, 'week'),
     (93, '3 maanden', 3, 'month'),
     (186, '6 maanden', 6, 'month'),
     (279, '9 maanden', 9, 'month'),
     (365, '1 jaar', 1, 'year'),
     (548, '1,5 jaar', 1.5 , 'year'),
     (730, '2 jaar', 2, 'year'),
     (1095, '3 jaar', 3, 'year'),
     (1460, '4 jaar', 4, 'year'),
     (1825, '5 jaar', 5, 'year'),
     (2190, '6 jaar', 6, 'year'),
     (2555, '7 jaar', 7, 'year'),
     (2920, '8 jaar', 8, 'year'),
     (3285, '9 jaar', 9, 'year'),
     (3650, '10 jaar', 10, 'year'),
     (4015, '11 jaar', 11, 'year'),
     (4380, '12 jaar', 12, 'year'),
     (4745, '13 jaar', 13, 'year'),
     (5110, '14 jaar', 14, 'year'),
     (5475, '15 jaar', 15, 'year'),
     (5840, '16 jaar', 16, 'year'),
     (6935, '19 jaar', 19, 'year'),
     (7300, '20 jaar', 20, 'year'),
     (7665, '21 jaar', 21, 'year'),
     (9125, '25 jaar', 25, 'year'),
     (10950, '30 jaar', 30, 'year'),
     (14600, '40 jaar', 40, 'year'),
     (18250, '50 jaar', 50, 'year'),
     (40150, '110 jaar', 110, 'year'),
     (99999, 'Bewaren', 50000, 'year');

COMMIT;
