BEGIN;

ALTER TABLE object_bibliotheek_entry
    ADD search_index tsvector;
ALTER TABLE object_bibliotheek_entry
    ADD searchable_id INTEGER NOT NULL DEFAULT nextval('searchable_searchable_id_seq');

ALTER TABLE searchable ALTER object_type SET DATA TYPE text;
ALTER TABLE object_bibliotheek_entry inherit searchable;

COMMIT;
