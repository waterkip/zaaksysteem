BEGIN;

    ALTER TABLE groups ADD COLUMN uuid UUID DEFAULT uuid_generate_v4();
    ALTER TABLE roles  ADD COLUMN uuid UUID DEFAULT uuid_generate_v4();

COMMIT;
