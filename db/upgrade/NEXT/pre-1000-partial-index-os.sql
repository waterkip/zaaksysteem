BEGIN;

    -- Be able to rerun this sql
    DROP INDEX IF EXISTS stuf_np_external_id_idx;
    -- Create a partial unique index:
    CREATE UNIQUE INDEX stuf_np_external_id_idx ON object_subscription (external_id, config_interface_id) WHERE date_deleted IS NULL and local_table = 'NatuurlijkPersoon' and external_id != 'IN_PROGRESS';

    -- Be able to rerun this sql
    DROP INDEX IF EXISTS stuf_np_local_id_idx;
    -- Create a partial unique index:
    CREATE UNIQUE INDEX stuf_np_local_id_idx ON object_subscription (local_id, config_interface_id) WHERE date_deleted IS NULL and local_table = 'NatuurlijkPersoon';

COMMIT;

