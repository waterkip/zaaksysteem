BEGIN;
    ALTER TABLE service_geojson_relationship ADD COLUMN last_modified TIMESTAMP WITH TIME ZONE;
COMMIT;