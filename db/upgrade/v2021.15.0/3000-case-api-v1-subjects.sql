BEGIN;

  ALTER TABLE zaak ADD COLUMN requestor_v1_json   JSONB;
  ALTER TABLE zaak ADD COLUMN assignee_v1_json    JSONB;
  ALTER TABLE zaak ADD COLUMN coordinator_v1_json JSONB;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    BEGIN

      SELECT INTO
        NEW.requestor_v1_json
        case_subject_json(hstore(zb))
      FROM
        zaak_betrokkenen zb
      WHERE
        zb.id = NEW.aanvrager;

      IF NEW.behandelaar IS NOT NULL THEN
        SELECT INTO
          NEW.assignee_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          zb.id = NEW.behandelaar;

      END IF;

      IF NEW.coordinator IS NOT NULL THEN
        SELECT INTO
          NEW.coordinator_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
      END IF;
      RETURN NEW;

    END;

  $$;

  DROP TRIGGER IF EXISTS update_subject_v1_json ON "zaak";

  CREATE TRIGGER update_subject_v1_json
     BEFORE INSERT
     OR UPDATE
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE update_subject_json();

  UPDATE zaak SET uuid = uuid;
  ALTER TABLE zaak ALTER COLUMN requestor_v1_json SET NOT NULL;

COMMIT;
