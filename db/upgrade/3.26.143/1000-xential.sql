

BEGIN;
    update interface set interface_config = jsonb_set(interface_config::jsonb, '{contextservice_require_base_id}', '1') where module = 'xential';
COMMIT;
