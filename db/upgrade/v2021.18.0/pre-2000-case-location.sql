BEGIN;

  CREATE OR REPLACE FUNCTION case_location_as_v0_json(
    IN location hstore,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;
    bid text;

    types text[];
    key text;
    brecord jsonb;

  BEGIN

    SELECT INTO value_json jsonb_build_object();

    types := ARRAY[
      'ligplaats',
      'nummeraanduiding',
      'openbareruimte',
      'pand',
      'standplaats',
      'verblijfsobject',
      'woonplaats'
    ];

    FOREACH btype in ARRAY types
    LOOP

      IF btype = 'ligplaats'
      THEN
          bid := location->'bag_ligplaats_id';
      ELSIF btype = 'nummeraanduiding'
      THEN
          bid := location->'bag_nummeraanduiding_id';
      ELSIF btype = 'openbareruimte'
      THEN
          bid := location->'bag_openbareruimte_id';
      ELSIF btype = 'pand'
      THEN
          bid := location->'bag_pand_id';
      ELSIF btype = 'verblijfsobject'
      THEN
          bid := location->'bag_verblijfsobject_id';
      ELSIF btype = 'woonplaats'
      THEN
          bid := location->'bag_woonplaats_id';
      ELSIF btype = 'standplaats'
      THEN
          bid := location->'bag_standplaats_id';
      END IF;

      -- we don't store it, so we just fill it here for now
      SELECT INTO value_json value_json || jsonb_build_object(
        CONCAT('case.correspondence_location.', btype), null
      );

      key := CONCAT('case.case_location.', btype);

      IF bid IS NULL
      THEN
        SELECT INTO value_json value_json || jsonb_build_object(
          key, null
        );
        CONTINUE;
      END IF;

      SELECT INTO brecord bag_type_id_to_json(btype, bid);

      SELECT INTO value_json value_json || jsonb_build_object(
        key, brecord
      );

    END LOOP;



  END;
  $$;

COMMIT;

